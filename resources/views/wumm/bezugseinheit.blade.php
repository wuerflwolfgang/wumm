@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.showing-all-users') !!}
@endsection

@section('template_linked_css')
    @if(config('usersmanagement.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('usersmanagement.datatablesCssCDN') }}">
    @endif
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {!! trans('wumm.showing-all-bezugseinheiten') !!}
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        {!! trans('usersmanagement.users-menu-alt') !!}
                                    </span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="/users/create">
                                        <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                                        {!! trans('usersmanagement.buttons.create-new') !!}
                                    </a>
                                    <a class="dropdown-item" href="/users/deleted">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                                        {!! trans('usersmanagement.show-deleted-users') !!}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        @if(config('usersmanagement.enableSearchUsers'))
                            @include('partials.search-bezugseinheit-form')
                        @endif

                        <div class="table-responsive users-table activity-table">
                            <table class="table table-striped table-condensed table-sm  table-hover  data-table">
                                <caption id="user_count">
                                    {{ trans_choice('wumm.bezugseinheit_total', 1, ['bezugseinheitcount' => count($bezugseinheiten)]) }}
                                </caption>
                                <thead >
                                    <tr>
                                        <th>  <span class="hidden-sm hidden-xs"><i class="fa fa-database fa-fw" aria-hidden="true"></i>
                                       
                                        {!! trans('wumm.id') !!}
                                        </span>
                                        </th>
                                        
                                        <th>{!! trans('wumm.name_bezugseinheit') !!}</th>
                                        <th>{!! trans('wumm.descr') !!}</th>
                                        <th class="hidden-xs">{!! trans('wumm.email') !!}</th>
                                        <th class="hidden-xs"><i class="fa fa-calendar-o " aria-hidden="true"></i>{!! trans('wumm.von') !!}</th>
                                        <th class="hidden-xs"><i class="fa fa-calendar-o " aria-hidden="true"></i>{!! trans('wumm.bis') !!}</th>
                                      
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('wumm.created') !!}</th>
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('wumm.updated') !!}</th>
                                        <th>{!! trans('usersmanagement.users-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($bezugseinheiten as $einheit)
                                        <tr>
                                            <td>{{$einheit->id}}</td>
                                            <td>{{$einheit->name}}</td>
                                            <td>{{$einheit->descr}}</td>
                                            <td class="hidden-xs"><a href="mailto:{{ $einheit->person[0]->email}}" title="email {{ $einheit->person[0]->email}}">{{ $einheit->person[0]->email }}</a></td>
                                            <td class="hidden-xs">{{$einheit->von}}</td>
                                            <td class="hidden-xs">{{$einheit->bis}}</td>
                                       
                                            <td class="hidden-sm hidden-xs hidden-md">{{$einheit->created_at}}</td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{$einheit->updated_at}}</td>
                                            <td>
                                                {!! Form::open(array('url' => 'bezugseinheit/' . $einheit->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('wumm.delete_bezugseinheit'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => trans('wumm.delete_bezugseinheit'), 'data-message' => trans('wumm.delete_bezugseinheit'))) !!}
                                                {!! Form::close() !!}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-success btn-block" href="{{ URL::to('bezugseinheit/' . $einheit->id) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('wumm.show_bezugseinheit') !!}
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('bezugseinheit/' . $einheit->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('wumm.edit_bezugseinheit') !!}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tbody id="search_results"></tbody>
                                @if(config('usersmanagement.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif

                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $bezugseinheiten->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($bezugseinheiten) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    @if(config('usersmanagement.enableSearchUsers'))
        @include('scripts.search-bezugseinheit')
    @endif
@endsection
