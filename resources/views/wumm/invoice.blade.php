<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WUMM - Rechnung</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<link rel="stylesheet" href="{{asset('css/app.css') }}">

   <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 50px;
			padding: 20px;
        }

        * {
            font-family: Ebrima, Arial, sans-serif;
			font-size: 13px;
        }
		
        a {
            color: #fff;
            text-decoration: none;
        }

		h1 {	
			font-weight: bold;
			font-size: 16px;
		}
		
		h2 {
			font-size: 12px;
			text-decoration: none;
			display: block;
			text-align: right;
		}
		
		h3 {
			font-size: 10px;
			color: #e2df22;
		}
		
		footer {
			padding: 10px 10px 10px 10px;
			background-color: #E2DF22;
			font-size: 12px;
		}
		
		hr {
          margin-top: 1rem;
          margin-bottom: 1rem;
          border: 0;
          height: 3px;
         
          background-color: #e2df22;
        }
        

        table {
          width: 100%;
        }

		
		.greenhead
		{
		    height: 40px;
			background-color: #e2df22 !important;
		}

		


    </style>


</head>
<body>
	<div class="information">
        <div style="text-align: right"><img src="{{ $path }}" alt="Logo" width="270" class="logo"/>
    </div>
	</div>
	<hr>
			<br>
			<br>
			<h3> WUMM GmbH | Allendestraße 68 | 98574 Schmalkalden </h3>
	<table width="160%" > 
        <tr>
			<td> Herrn </td>
			<td><i aria-hidden="true" class="fa fa-user fa-fw"></i> Stefan Haberzettl </td>
		</tr>
		<tr>
			<td> {{ $person->first_name }} {{ $person->last_name }} </td>
			<td><i class="fa fa-envelope"></i>  sha@wumm.energy</td>
		</tr>
		<tr>
			<td> {{ $person->street }} </td>
			<td> <i aria-hidden="true" class="fa fa-user fa-fw"></i> WUMM.Box Nummer </td>
		</tr>
		<tr>
			<td> {{ $person->zip }} {{ $person->city }} </td>
			<td><i aria-hidden="true" class="fa fa-calendar fa-fw"></i>  Datum: {{ date('d.m.Y') }} </td>
		</tr>
 	</table>
	<br>
	<table width="70%" > 
        <tr>
			<td> Ihre Kundennummer: </td>
			<td> Ihre Vertragsnummer: </td>
		</tr>
		<tr>
			<td> K00037 </td>
			<td> WUMM.Box00007 </td>
		</tr>
		<tr>
			<td> Leistungsempfänger </td>
		</tr>
		<tr>
			<td> {{ $person->firstname}} {{ $person->last_name }} </td>
		</tr>
 	</table>	


<br>
<br>

	 <h1>Verbrauchsabrechnung für den Zeitraum vom <b>{{ $von }}</b> bis <b>{{ $bis }}</b>
		<br> Rechnungsnummer: WUMM.Box2020Strom00011 </h1>
		<br>
	
	<p> Sehr geehrter Herr {{ $person->last_name }}, <br>
		Gemäß Stromliefervertrag berechnen wir Ihnen für die oben genannte Verbrauchsstelle wie folgt:
	</p>
<br>

<table >
  
 
    <thead id="greenhead" class="greenhead" >
    <tr>
      <th scope="col"></th>
      <th scope="col">Nettobetrag in Euro</th>
      <th scope="col">Umsatzsteuer in Euro</th>
      <th scope="col">Bruttobetrag in Euro</th>
    </tr>
  </thead>
 
  <tbody>
    <tr>
      <th scope="row">Abrechnung WUMM.Box</th>
      <td>-</td>
      <td>-</td>
      <td>-</td>
    </tr>
    <tr>
      <th scope="row">Geleistete Abschlagszahlung</th>
      <td>-</td>
      <td>-</td>
      <td>-</td>
    </tr>
	<tr>
		 <th scope="row">Forderung/Guthaben</th>
		 <td>-</td>
		 <td>-</td>
		 <td>-</td>
	</tr>
 </tbody>
</table>
	<h2> *Negativbeträge sind Guthaben </h2>
	</br>
<p> Wir nutzen zum Ausgleich der Forderung/Guthaben die von Ihnen angegebene Bankverbindung </br> unter Angabe unserer Mandatsreferenznummer. </p>
<br>
<br>
<br>
<br>
<br>

<div class="invoice">
    <h1>Verbrauch für Zählernummer #{{ $name }}</h1>
   
   
    <table >
    	
            <thead id="greenhead" class="greenhead"  >
            <tr>
                <th> Von </th>
                <th> Bis </th>
    			<th> Stand alt </th>
    			<th> Stand neu </th> 
                <th> Menge in kWh </th>
            </tr>
            </thead>
      
        <tbody>
     
     
         <tr>
            <td>{{  $von }}</td>
            <td>{{  $bis }}</td>
            <td>{{  $alt }}</td>
            <td>{{  $neu }}</td>
            <td>{{  $amount }}</td>
            
        </tr>
  

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="left">Total</td>
            <td align="left" class="gray">{{ $amount }} {{ $unit}}</td>
        </tr>
        </tfoot>
    </table>
</div>

<div class="col-xs-12" style="height:300px;"></div>
</body>
<footer>
	<table width="100%"  > 
        <tr>
			<td> WUMM GmbH </td>
			<td> <i aria-hidden="true" class="fa fa-phone fa-fw"></i>Tel.: 03683 46490000 </td>
			<td> Handelsregister: AG Jena HRB 514747 </td>
			<td> Kontoinhaber: WUMM GmbH </td>
		</tr>
		<tr>
			<td> Allendestraße 68 </td>
			<td> <i aria-hidden="true" class="fa fa-mobile fa-fw"></i>Handy: 0178 9299086 </td>
			<td> <i aria-hidden="true" class="fa fa-user fa-fw"></i>Geschäftsführer: Udo Fischer </td>
			<td> IBAN: DE29 7715 0000 0101 657153 </td>
		</tr>
		<tr>
			<td> 98574 Schmalkalden </td>
			<td> <i aria-hidden="true" class="fa fa-envelope fa-fw"></i>E-Mail.: info@wumm.energy </td>
			<td> Werner Maier </td>
			<td> BIC: BYLADEM1KUB </td>
		</tr>
		<tr>
			<td> UStID: DE28 4129 949 </td>
			<td> <i aria-hidden="true" class="fa fa-firefox fa-fw"></i>www.wumm.energy </td>
			<td></td>
			<td> Sparkasse Kulmbach- Kronach </td>
		</tr>
 	</table>
</footer>




</html>