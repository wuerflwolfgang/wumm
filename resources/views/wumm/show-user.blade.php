@extends('layouts.app')

@section('template_title')
  {!! trans('wumm.details_bezugseinheit', ['name' => $einheit->name]) !!}
@endsection


@php $id_bezugseinheit = $einheit->id  @endphp

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">

        <div class="card">

          <div class="card-header text-white @if ($einheit->id == 1) bg-success @else bg-danger @endif">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              {!! trans('wumm.details_bezugseinheit', ['name' => $einheit->name]) !!}
              <div class="float-right">
                <a href="{{ route('bezugseinheit') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('wumm.back_to_unit') }}">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('wumm.back_to_unit') !!}
                </a>
              </div>
            </div>
          </div>

          <div class="card-body">

            <div class="row">

              <div class="col-sm-4 col-md-6">
                <h4 class="text-muted margin-top-sm-1 text-center text-left-tablet">
                  {{ $einheit->name }}
                </h4>
                <p class="text-center text-left-tablet">
                  <strong>
                    {{ $einheit->id }} {{ $einheit->name }}
                  </strong>

                  @if($einheit->ctrluser)
                    <br />
                    <span class="text-center" data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.email-user', ['user' => $einheit->name]) }}">
                      {{ Html::mailto($einheit->person[0]->email, $einheit->person[0]->email) }}
                    </span>
                  @endif
                </p>
                @if ($einheit->id)
                  <div class="text-center text-left-tablet mb-4">
                    <a href="{{ url('/profile/'.$einheit->name) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="{{ trans('usersmanagement.viewProfile') }}">
                      <i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md"> {{ trans('usersmanagement.viewProfile') }}</span>
                    </a>
                    <a href="/users/{{$einheit->id}}/edit" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.editUser') }}">
                      <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md"> {{ trans('usersmanagement.editUser') }} </span>
                    </a>
                    {!! Form::open(array('url' => 'users/' . $einheit->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => trans('usersmanagement.deleteUser'))) !!}
                      {!! Form::hidden('_method', 'DELETE') !!}
                      {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md">' . trans('usersmanagement.deleteUser') . '</span>' , array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user?')) !!}
                    {!! Form::close() !!}
                  </div>
                @endif
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            @if ($einheit->person[0]->company_name )

              <div class="col-sm-5 col-6 text-larger">
                <strong>
                  {{ trans('wumm.company_name') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $einheit->person[0]->company_name }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            @if (true)

            <div class="col-sm-5 col-6 text-larger">
              <strong>
                {{ trans('usersmanagement.labelEmail') }}
              </strong>
            </div>

            <div class="col-sm-7">
              <span data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.email-user', ['user' => $einheit->name]) }}">
                {{ HTML::mailto($einheit->person[0]->email,$einheit->person[0]->email) }}
              </span>
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            @endif

            @if ($einheit->person[0]->first_name )

              <div class="col-sm-5 col-6 text-larger">
                <strong>
                  {{ trans('usersmanagement.labelFirstName') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $einheit->person[0]->first_name }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            @if ($einheit->person[0]->last_name )

              <div class="col-sm-5 col-6 text-larger">
                <strong>
                  {{ trans('usersmanagement.labelLastName') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $einheit->person[0]->last_name }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            <div class="col-sm-5 col-6 text-larger">
              <strong>
                {{ trans('wumm.assigned_messeinheiten') }}
              </strong>
            </div>
            
             <div class="col-sm-7">
                {{ count($messeinheiten) }}
             </div>


            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            <div class="col-sm-5 col-6 text-larger">
              <strong>
                {{ trans('usersmanagement.labelStatus') }}
              </strong>
            </div>

            <div class="col-sm-7">
              @if ($einheit->id == 1)
                <span class="badge badge-success">
                  Activated
                </span>
              @else
                <span class="badge badge-danger">
                  Not-Activated
                </span>
              @endif
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            <div class="col-sm-5 col-6 text-larger">
              <strong>
                {{ trans('usersmanagement.labelAccessLevel')}} {{ $levelAmount }}:
              </strong>
            </div>

           
            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            <div class="col-sm-5 col-6 text-larger">
              <strong>
                {{ trans('usersmanagement.labelPermissions') }}
              </strong>
            </div>

          

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            @if ($einheit->created_at)

              <div class="col-sm-5 col-6 text-larger">
                <strong>
                  {{ trans('usersmanagement.labelCreatedAt') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $einheit->created_at }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            @if ($einheit->updated_at)

              <div class="col-sm-5 col-6 text-larger">
                <strong>
                  {{ trans('usersmanagement.labelUpdatedAt') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $einheit->updated_at }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif
            
                                    <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
                                    {{ trans_choice('wumm.bezugseinheit_total', 1, ['bezugseinheitcount' => count($messeinheiten)]) }}
                                </caption>
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('wumm.id') !!}</th>
                                        <th>{!! trans('wumm.name_messeinheit') !!}</th>
                                        <th>{!! trans('wumm.descr') !!}</th>

                                        <th class="hidden-xs">{!! trans('wumm.von') !!}</th>
                                        <th class="hidden-xs">{!! trans('wumm.bis') !!}</th>
                                      
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('wumm.created') !!}</th>
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('wumm.updated') !!}</th>
                                        <th>{!! trans('usersmanagement.users-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($messeinheiten as $einheit)
                                        <tr>
                                            <td>{{$einheit->id}}</td>
                                            <td>{{$einheit->name}}</td>
                                            <td>{{$einheit->descr}}</td>
                                            
                                            <td class="hidden-xs">{{$einheit->von}}</td>
                                            <td class="hidden-xs">{{$einheit->bis}}</td>
                                       
                                            <td class="hidden-sm hidden-xs hidden-md">{{$einheit->created_at}}</td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{$einheit->updated_at}}</td>
                                            <td>
                                                {!! Form::open(array('url' => 'messeinheit/' . $einheit->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('wumm.delete_messeinheit'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => trans('wumm.delete_bezugseinheit'), 'data-message' => trans('wumm.really_delete_bezugseinheit'))) !!}
                                                {!! Form::close() !!}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-success btn-block" href="{{ URL::to('messeinheit_detail/' . $einheit->id) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('wumm.show_messeinheit') !!}
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('messeinheit' . $einheit->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('wumm.edit_messeinheit') !!}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tbody id="search_results"></tbody>
                                @if(config('usersmanagement.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif

                            </table>

                            @if(config('usersmanagement.enablePagination', true))
                                {{ $messeinheiten->links() }}
                            @endif

                        </div>
            
            
            
            
            




       

          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @if(config('usersmanagement.tooltipsEnabled'))
    @include('scripts.tooltips')
  @endif
@endsection
