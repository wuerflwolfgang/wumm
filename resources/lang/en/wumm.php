<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'errorBezugseinheitDeleted' => 'There are existing references on this dataset, cannot delete!!!',
    'name_bezugseinheit'        => 'BDEW Codenr.',
    'descr'                     => 'Description',
    'created'                   => 'Created',
    'updated'                   => 'Updated',
    'delete_bezugseinheit'      => 'Delete Unit',
    'show_bezugseinheit'        => 'Show Details',
    'edit_bezugseinheit'        => 'Edit BE',
    'mainmenu'                  => 'WUMM Menue',
    'messdaten'                 => 'Messdaten',
    'messeinheit'               => 'Messeinheit',
    'bezugseinheit'             => 'Bezugseinheit',
    'showing-all-bezugseinheiten' => 'Show all Units',
    'bezugseinheit_total'       => '{1} :bezugseinheitcount Units total|[2,*] :bezugseinheitcount total units',
    'id'                        => 'ID',
    'email'                     => 'Email',
    'von'                       => 'Von',
    'bis'                       => 'Bis',
    'details_bezugseinheit'     => ':name\'s Unit Details',
    'back_to_unit'              => 'Back to Unit',
    'really_delete_bezugseinheit' => 'Are you sure to delete this unit?',
    'assigned_messeinheiten'    => 'Total assigned Units:',
    'name_messeinheit'          => 'Measuring Unit',
    'delete_messeinheit'        => 'Delete',
    'edit_messeinheit'          => 'Edit',
    'show_messeinheit'          => 'Show',
    'amount'                    => 'Amount',
    'unit'                      => 'Unit',
    'messstelle'                => 'Measuring Unit',
    'messdaten_total'           => '{1} :messdatencount samples total|[2,*] :messdatencount total samples',
    'pending_files'             => 'There are :pendingfiles unprocessed files',
    'import_verify'             => 'Please verify import stats!!',
    'dashboard_overview'        => 'Wumm Import Stats',
    'filename'                  => 'Filename',
    'summary'                   => 'Summary',
    'company_name'              => 'Company Name'

    
];
