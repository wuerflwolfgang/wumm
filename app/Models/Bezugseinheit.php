<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bezugseinheit
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descr
 * @property string $ctrluser
 * @property string $typ
 *
 * @package App\Models
 */
class Bezugseinheit extends Model
{
	protected $table = 'bezugseinheit';

	protected $fillable = [
		'descr',
		'ctrluser',
		'typ'
	];
	
	  
    public function messeinheit()
    {
        return $this->belongsToMany(Messeinheit::class)->using(BezugseinheitMesseinheit::class);
    }
    
    
    public function person()
    {
        return $this->hasMany(Person::class);
    }
}
