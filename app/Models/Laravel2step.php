<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Laravel2step
 * 
 * @property int $id
 * @property int $userId
 * @property string $authCode
 * @property int $authCount
 * @property bool $authStatus
 * @property Carbon $authDate
 * @property Carbon $requestDate
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property User $user
 *
 * @package App\Models
 */
class Laravel2step extends Model
{
	protected $table = 'laravel2step';

	protected $casts = [
		'userId' => 'int',
		'authCount' => 'int',
		'authStatus' => 'bool'
	];

	protected $dates = [
		'authDate',
		'requestDate'
	];

	protected $fillable = [
		'userId',
		'authCode',
		'authCount',
		'authStatus',
		'authDate',
		'requestDate'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'userId');
	}
}
