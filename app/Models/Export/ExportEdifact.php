<?php
namespace App\Models\Export;

use Illuminate\Support\Facades\Log;


/**
 *
 * @author wolfgang
 *        
 */
class ExportEdifact 
{

        /**
     * The name and signature of the console command.
     *
     * @var string
     */

    /**
     * Relative path to storage path
     */

    protected $importPath = '/app/export/edifact/';
   

    /*
     * IEdifactImportPlugin 
     */
    protected $driver = null;

    /**
     * console driver
     */
    protected $console = null;
    
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edifact Exporter';
    
    
    
    //
    public function __construct(\Illuminate\Console\Command $console=null)
    {
        if ($console)
        {
            $this->console = $console;
        }
    }

    /**
     * set path relative to storage path
     */
    
    public function setExportPath(string $path)
    {
       if (substr($path, -1) != '/') $path .= '/';
       $this->exportPath = $path;    
    }
    
    public function getExportPath()
    {
        return $this->exportPath;
    }

    public function processData(array $data)
    {

        $type           = $data['type'];
        $ns             = "\\App\\Models\\Export\\Plugins\\".$type;
               
        Log::info("Loading Plugins $ns");
        if (isset($this->console))
        {
            $this->console->line("<fg=default;bg=black></><fg=green;bg=black>Loading Export Plugin <fg=default;bg=black> $ns <fg=default;bg=black>");
        }
        $this->driver          = new $ns();
        $this->driver->console =  $this->console;
        $this->driver->exportData($data);
    }
    
    

}
