<?php
namespace App\Models\Export\Plugins;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use EDI\Parser;
use Faker\Factory;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;
use App\Models\Export\Plugins\IEdifactExportPlugin;

class Z16 implements IEdifactExportPlugin
{
    
    protected $melo        = null;
    protected $receiver    = null;
    protected $sender      = null;
    protected $messeinheit = null;
    
    protected $exportPath = '/app/export/edifact/exported/';
    
    const UNIT = 16;



    protected $edifact_template_header = '

    [
        [
          "UNB",
          [
            "UNOC",
            "3"
          ],
          [
            "9900259000002",
            "500"
          ],
          [
            "9911921000006",
            "500"
          ],
          [
            "200209",
            "0203"
          ],
          "N06669940A",
          "",
          "TL"
        ],
        [
          "UNH",
          "1",
          [
            "MSCONS",
            "D",
            "04B",
            "UN",
            "2.3"
          ]
        ],
        [
          "BGM",
          "Z16",
          "N06669940A-1",
          "9"
        ],
        [
          "DTM",
          [
            "137",
            "202002090203",
            "203"
          ]
        ],
        [
          "RFF",
          [
            "Z13",
            "13011"
          ]
        ],
        [
          "NAD",
          "MS",
          [
            "9900259000002",
            "",
            "293"
          ]
        ],
        [
          "NAD",
          "MR",
          [
            "9911921000006",
            "",
            "293"
          ]
        ],
        [
          "UNS",
          "D"
        ],
        [
          "NAD",
          "DED"
        ],
        [
          "LOC",
          "Z06",
          "HN0"
        ],
        [
          "DTM",
          [
            "293",
            "20150916133010",
            "204"
          ]
        ],
        [
          "DTM",
          [
            "157",
            "201601",
            "610"
          ]
        ],
        [
          "LIN",
          "0"
        ],
        [
          "PIA",
          "5",
          [
            "1-1:9.99.0",
            "Z02"
          ]
        ]
    ]
    ';
    
    
    protected $edifact_template_footer = '  
         [
            "UNT",
            "3246",
            "1"
          ],
          [
            "UNZ",
            "1",
            "N06669940A"
          ]
        ';












    
    public function exportData(array $data)
    {
        var_dump($data);
        echo "Z16";
    }
    
    
}

