<?php
namespace App\Models\Export\Plugins;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use EDI\Parser;
use Faker\Factory;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;
use App\Models\Export\Plugins\IEdifactExportPlugin;

abstract class AbstractMessage 
{
  
    protected  $exportPath = '/app/export/edifact/';
    /**
     * 
     * @param unknown $name
     * @return string[]|string[][]
     */
    
    public function getSegment($name)
    {
        if (isset($this->template[$name]) && is_array($this->template[$name]) ) return $this->template[$name];
        return false;
    }
    
    /**
     * 
     * @param unknown $name
     * @param array $segment
     * @return boolean
     */
    
    public function setSegment($name, array $segment)
    {
        if (isset($this->template[$name])) 
        {
            $this->template[$name] = $segment;
            return true;
        }
        return false;
    }
    
    
    public function exportData(array $data)
    {
         return $this->template;
    }
    
    /**
     * set path relative to storage path
     */
    
    public function setExportPath(string $path)
    {
        if (substr($path, -1) != '/') $path .= '/';
        $this->exportPath = $path;
    }
    
    public function getExportPath()
    {
        return $this->exportPath;
    }
    
    public function saveFile(string $data, $filename = null)
    {
        $path_prefix   = storage_path();
        $filename      = $filename ? $filename : bin2hex(random_bytes(20)).'_random.txt';
        $complete_path = $path_prefix.$this->getExportPath().$filename;
        
        if (file_exists($complete_path))
        {
            $filename_new  = bin2hex(random_bytes(20)).'_random.txt';
            $complete_path = $path_prefix.$this->getExportPath().$filename_new;
            Log::alert("Filename with name $filename already exists, creating random filename $filename_new");
        }
       
        file_put_contents($complete_path, $data);
    }

    
}