<?php
namespace App\Models\Export\Plugins;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use EDI\Parser;
use Faker\Factory;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;
use App\Models\Export\Plugins\IEdifactExportPlugin;
use App\Models\Export\Plugins\AbstractMessage;

class ControlMessage extends AbstractMessage implements IEdifactExportPlugin
{
    
    protected $melo        = null;
    protected $receiver    = null;
    protected $sender      = null;
    protected $messeinheit = null;
    
    protected $exportPath = '/app/export/edifact/exported/';
    
    const UNIT = 16;
    
    protected $template = array (
        'UNB' =>
        array (
            0 => 'UNB',
            1 =>
            array (
                0 => 'UNOC',
                1 => '3',
            ),
            2 =>
            array (
                0 => '9900259000002',
                1 => '500',
            ),
            3 =>
            array (
                0 => '9911921000006',
                1 => '500',
            ),
            4 =>
            array (
                0 => '200303',
                1 => '1125',
            ),
            5 => '4545455',
        ),
        'UNH' =>
        array (
            0 => 'UNH',
            1 => '2432051',
            2 =>
            array (
                0 => 'CONTRL',
                1 => 'D',
                2 => '3',
                3 => 'UN',
                4 => '2.0a',
            ),
        ),
        'UCI' =>
        array (
            0 => 'UCI',
            1 => '1116641',
            2 =>
            array (
                0 => '9911921000006',
                1 => '500',
            ),
            3 =>
            array (
                0 => '9900259000002',
                1 => '500',
            ),
            4 => '4',
            5 => '7',
            6 => 'UNB',
            7 =>
            array (
                0 => '4',
                1 => '1',
            ),
        ),
        'UNT' =>
        array (
            0 => 'UNT',
            1 => '3',
            2 => '2432051',
        ),
        'UNZ' =>
        array (
            0 => 'UNZ',
            1 => '1',
            2 => '4545455',
        ),
    );
    

    public function setSender($sender)
    {
        
        $this->template['UNB'][3][0] = $sender;
        
    }
    
    public function setReceiver($receiver)
    {
        $this->template['UNB'][2][0] = $receiver;
    }
    
    /**
     * 
     */
    public function setSenderDate($date = null)
    {
        // preset with current time
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        
        $hour    = date('H');
        $minute  = date('m');
        
        if ($date)
        {
            $year  = date('y', $date);
            $month = date('m', $date);
            $day   = date('d', $date);
            
            $hour    = date('H', $date);
            $minute  = date('m', $date);
           
        }
        
        $this->template['UNB'][4][0] = $year.$month.$day;
        $this->template['UNB'][4][1] = $hour.$minute;
        
    }
    
    
    public function setMessageReference(string $ref)
    {
        $this->template['UCI'][1] = $ref;
    }
    
    public function exportData(array $data)
    {
         return $this->template;
    }
    
    /*
     * 
     */
    
    public function setUNHId()
    {
        
        $random_number = str_pad(mt_rand(10,1000000), 7, '0', STR_PAD_LEFT);

        // UNH reference, closed in UNT
        $this->template['UNH'][1] =  $random_number;
        $this->template['UNT'][2] =  $random_number;
        return $random_number;
    }
    
    /**
     * 
     * 
     * @param unknown $referenz
     * @return string
     */
    
    public function setUNBId($referenz = null)
    {
        // UNB reference, closed in UNZ
        $random_number = str_pad(mt_rand(10,1000000), 7, '0', STR_PAD_LEFT);
        $this->template['UNB'][5] = $random_number;
        $this->template['UNZ'][2] = $random_number;
        return $random_number;
    }

    
}