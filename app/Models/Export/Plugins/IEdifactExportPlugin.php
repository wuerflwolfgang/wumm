<?php
namespace App\Models\Export\Plugins;

interface IEdifactExportPlugin
{
    
    public function exportData(array $data);
}

