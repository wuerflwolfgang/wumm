<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class BezugseinheitMesseinheit
 * 
 * @property int $bezugseinheit_id
 * @property int $messeinheit_id
 *
 * @package App\Models
 */
class BezugseinheitMesseinheit extends Pivot
{
	
}
