<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LaravelLoggerActivity
 * 
 * @property int $id
 * @property string $description
 * @property string $userType
 * @property int $userId
 * @property string $route
 * @property string $ipAddress
 * @property string $userAgent
 * @property string $locale
 * @property string $referer
 * @property string $methodType
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class LaravelLoggerActivity extends Model
{
	use SoftDeletes;
	protected $table = 'laravel_logger_activity';

	protected $casts = [
		'userId' => 'int'
	];

	protected $fillable = [
		'description',
		'userType',
		'userId',
		'route',
		'ipAddress',
		'userAgent',
		'locale',
		'referer',
		'methodType'
	];
}
