<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LaravelBlocker
 * 
 * @property int $id
 * @property int $typeId
 * @property string $value
 * @property string $note
 * @property int $userId
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property LaravelBlockerType $laravel_blocker_type
 *
 * @package App\Models
 */
class LaravelBlocker extends Model
{
	use SoftDeletes;
	protected $table = 'laravel_blocker';

	protected $casts = [
		'typeId' => 'int',
		'userId' => 'int'
	];

	protected $fillable = [
		'typeId',
		'value',
		'note',
		'userId'
	];

	public function laravel_blocker_type()
	{
		return $this->belongsTo(LaravelBlockerType::class, 'typeId');
	}
}
