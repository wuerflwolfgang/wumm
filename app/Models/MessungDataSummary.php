<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MessungDataSummary
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descr
 * @property string $filename
 * @property string $sum
 * @property Carbon $von
 * @property Carbon $bis
 * 
 * @property Collection|MessungDatum[] $messung_data
 *
 * @package App\Models
 */
class MessungDataSummary extends Model
{
	protected $table = 'messung_data_summary';

	protected $dates = [
		'von',
		'bis'
	];

	protected $fillable = [
		'descr',
		'filename',
		'sum',
		'von',
		'bis'
	];

	public function messung_data()
	{
		return $this->hasMany(MessungData::class, 'summary_id');
	}
}
