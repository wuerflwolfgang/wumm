<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Messeinheit
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $messstelle_id
 * @property string $typ
 * @property string $descr
 * @property string $ctrluser
 * @property Carbon $ctrldate
 * @property string $ctrlstate
 * 
 * @property Messstelle $messstelle
 * @property Collection|MessungDatum[] $messung_data
 *
 * @package App\Models
 */
class Messeinheit extends Model
{
	protected $table = 'messeinheit';

	protected $casts = [
		'messstelle_id' => 'int'
	];

	protected $dates = [
		'ctrldate'
	];

	protected $fillable = [
		'messstelle_id',
		'typ',
		'descr',
		'ctrluser',
		'ctrldate',
		'ctrlstate'
	];
	
	public function bezugseinheit()
	{
	    return $this->belongsToMany(Bezugseinheit::class)->using(BezugseinheitMesseinheit::class);
	}

	public function messstelle()
	{
		return $this->belongsTo(Messstelle::class);
	}

	public function messung_data()
	{
		return $this->hasMany(MessungData::class);
	}
}
