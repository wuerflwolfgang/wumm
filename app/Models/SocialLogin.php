<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialLogin
 * 
 * @property int $id
 * @property int $user_id
 * @property string $provider
 * @property string $social_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property User $user
 *
 * @package App\Models
 */
class SocialLogin extends Model
{
	protected $table = 'social_logins';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'provider',
		'social_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
