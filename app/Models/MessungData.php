<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MessungDatum
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $von
 * @property Carbon $bis
 * @property Carbon $datum
 * @property int $messeinheit_id
 * @property float $amount
 * @property string $typ
 * @property string $descr
 * @property string $ctrluser
 * @property Carbon $ctrldate
 * @property string $ctrlstate
 * 
 * @property Messeinheit $messeinheit
 *
 * @package App\Models
 */
class MessungData extends Model
{
	protected $table = 'messung_data';

	protected $casts = [
		'messeinheit_id' => 'int',
	    'summary_id'     => 'int',
		'amount'         => 'float'
	];

	protected $dates = [
		'von',
		'bis',
		'datum',
		'ctrldate'
	];

	protected $fillable = [
		'von',
		'bis',
		'datum',
		'messeinheit_id',
	    'summary_id',
		'amount',
		'typ',
		'descr',
		'ctrluser',
		'ctrldate',
		'ctrlstate'
	];

	public function messeinheit()
	{
		return $this->belongsTo(Messeinheit::class);
	}
	
	public function summary()
	{
	    return $this->belongsTo(MessungDataSummary::class);
	}
}
