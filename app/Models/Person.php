<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Person
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $bezugseinheit_id
 * @property string $company_name
 * @property string $first_name
 * @property string $last_name
 * @property string $zip
 * @property string $city
 * @property string $street
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property bool $activated
 * @property string $token
 * @property string $type
 * @property string $deleted_at
 * 
 * @property Bezugseinheit $bezugseinheit
 *
 * @package App\Models
 */
class Person extends Model
{
	use SoftDeletes;
	protected $table = 'person';

	protected $casts = [
		'bezugseinheit_id' => 'int',
		'activated' => 'bool'
	];

	protected $hidden = [
		'password',
		'remember_token',
		'token'
	];

	protected $fillable = [
		'bezugseinheit_id',
		'company_name',
		'first_name',
		'last_name',
		'zip',
		'city',
		'street',
		'email',
		'password',
		'remember_token',
		'activated',
		'token',
		'type'
	];

	public function bezugseinheit()
	{
		return $this->belongsTo(Bezugseinheit::class);
	}
}
