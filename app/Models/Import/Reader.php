<?php
namespace App\Models\Import;

use EDI\Reader as EdiReader;
/**
 *
 * @author wolfgang
 *        
 */
class Reader extends EdiReader
{

    public function readEdiDataValueWumm($filter, int $l1, $l2 = false, bool $required = false)
    {
       
        // interpret filter parameters
        if (\is_array($filter)) {
            $segment_name = $filter[0];
            $filter_elements = $filter[1];
        } else {
            $segment_name = $filter;
            $filter_elements = false;
        }
        
        $segment = false;
        $segment_count = 0;
        var_dump("segment Name:".$segment_name);
       
        // search segment, who conform to filter
        foreach ($this->getParsedFile() as $edi_row) {
            if ($edi_row[0] == $segment_name) {
                var_dump("gefunden");
                var_dump($edi_row[0]);
                if ($filter_elements) {
                    $filter_ok = false;
                    foreach ($filter_elements as $el_id => $el_value) {
                        $f_el_list = \explode('.', (string)$el_id);
                        if (\count($f_el_list) === 1) {
                            if (
                                isset($edi_row[$el_id])
                                &&
                                $edi_row[$el_id] == $el_value
                                ) {
                                    $filter_ok = true;
                                    break;
                                }
                        } else if (
                            isset($edi_row[$f_el_list[0]])
                            &&
                            (
                                (
                                    isset($edi_row[$f_el_list[0]][$f_el_list[1]])
                                    &&
                                    \is_array($edi_row[$f_el_list[0]])
                                    &&
                                    $edi_row[$f_el_list[0]][$f_el_list[1]] == $el_value
                                    )
                                ||
                                (
                                    isset($edi_row[$f_el_list[0]])
                                    &&
                                    \is_string($edi_row[$f_el_list[0]])
                                    &&
                                    $edi_row[$f_el_list[0]] == $el_value
                                    )
                                )
                            ) {
                                $filter_ok = true;
                                break;
                            }
                    }
                    
                    if ($filter_ok === false) {
                        continue;
                    }
                }
                
                $segment = $edi_row;
                $segment_count++;

            }
        }
        
        // no found segment
        if (!$segment) {
            if ($required) {
                $this->errors[] = 'Segment "' . $segment_name . '" no exist';
            }
            
            return null;
        }
        
        // found more one segment - error
        if ($segment_count > 1) {
            
            $this->errors[] = 'Segment "' . $segment_name . '" is ambiguous';
            
            return null;
        }
        
        // validate elements
        if (!isset($segment[$l1])) {
            if ($required) {
                $this->errors[] = 'Segment value "' . $segment_name . '[' . $l1 . ']" no exist';
            }
            
            return null;
        }
        
        // requested first level element
        if ($l2 === false) {
            return $segment[$l1];
        }
        
        // requested second level element, but not exist
        if (!\is_array($segment[$l1]) || !isset($segment[$l1][$l2])) {
            if ($required) {
                $this->errors[] = 'Segment value "' . $segment_name . '[' . $l1 . '][' . $l2 . ']" no exist';
            }
            
            return null;
        }
        
        // second level element
        return $segment[$l1][$l2];
        
        
    }
    
    
    public function readGroupsWumm(string $before, string $start, string $end, string $after)
    {
        // init
        $groups = [];
        $group = [];
        $position = 'before_search';
       
        foreach ($this->getParsedFile() as $edi_row) {
            
          
            // search before group segment
            if ($position == 'before_search' && $edi_row[0] == $before) {
                $position = 'before_is';
                continue;
            }
            
            if ($position == 'before_search') {
                continue;
            }
            
            if ($position == 'before_is' && $edi_row[0] == $before) {
                continue;
            }
            var_dump($position);
            var_dump($edi_row[0]);
            // after before search start
            if ($position == 'before_is' && $edi_row[0] == $start) {
                var_dump("gefunden");
                $position = 'group_is';
                $group[] = $edi_row;
                continue;
            }
            
            // if after before segment no start segment, search again before segment
            if ($position == 'before_is') {
                $position = 'before_search';
                continue;
            }
            
            // get group element
            if ($position == 'group_is' && $edi_row[0] != $end) {
                $group[] = $edi_row;
                continue;
            }
            
            // found end of group
            if ($position == 'group_is' && $edi_row[0] == $end) {
                $position = 'group_finish';
                $group[] = $edi_row;
                $groups[] = $group;
                $group = [];
                continue;
            }
            
            // next group start
            if ($position == 'group_finish' && $edi_row[0] == $start) {
                $group[] = $edi_row;
                $position = 'group_is';
                continue;
            }
            
            // finish
            if ($position == 'group_finish' && $edi_row[0] == $after) {
                break;
            }
            
            $this->errors[] = 'Reading group ' . $before . '/' . $start . '/' . $end . '/' . $after
            . '. Error on position: ' . $position;
            
            return false;
        }
        
        return $groups;
    }
    
    
}

