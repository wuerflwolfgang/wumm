<?php
namespace App\Models\Import;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ImportKiwiGrid
{
    
    
    /*
     * IEdifactImportPlugin
     */
    protected $driver  = null;
    
    protected $console = null;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiwi Grid Importer';
    
    
    const  OAUTHENDPOINT="https://auth.staging.kiwigrid.com/access_token_app";
    const  AUTHENTICATORENDPOINT="https://cloud.staging.kiwigrid.com/rest/com.kiwigrid.service.auth/authenticator/accessToken";
    const  TIMESERIESREADSERVICEENDPOINT="https://cloud.staging.kiwigrid.com/rest/timeseriesreadservice/devices";
    const  USER="wumm_user";
    const  CHANNEL="wumm";
    const  CLIENT_ID="wumm.developer_app";
    const  CLIENT_SECRET="yF3H4wSs4M2dJq7RsxsP";
    const  DEVICE_GUID="406d474c-7cc9-4727-a9fb-4a78c1ad9d92";
    const  TAG_NAME="WorkCharge";
    
    const RUN_TIMESERIESREADSERVICE_GETTAGVALUEBOUNDARIES   =   "true";
    const RUN_TIMESERIESREADSERVICE_GETTAGVALUEATORBEFORE   =   "true";
    const RUN_TIMESERIESREADSERVICE_AGGREGATETAGVALUES      =   "true";
    
    
    //1577836800
    protected $timeboundary_from   = "1580544606";
    //
    
    //1581451140
    protected $timeboundary_end    = "1582099806";
    //
    protected $aggregation_config  = [
        'resolution'            => 'PT240M',
        'function'              => 'TWA',
        'fill'                  => 'NONE',
        'useCreatedAtAsValue'   => false,
        'timeZoneId'            => 'UTC',
    ];
    
    
    
    
    
    //
    public function __construct(\Illuminate\Console\Command $console=null)
    {
        if ($console)
        {
            $this->console = $console;
        }
    }
    
    
    
    
    /**
     *
     * @param string $from with format 2020-01-05 19:10:05
     * @param string $to
     */
    public function setBoundariesFromTo(string $from, string $to)
    {
        $this->timeboundary_from =  \DateTime::createFromFormat("Y-m-d H:i:s", $from)->getTimestamp();
        $this->timeboundary_end  =  \DateTime::createFromFormat("Y-m-d H:i:s", $to)->getTimestamp();
    }
    
    
    /**
     * 
     * @param string $from with format 2020-01-05 19:10:05
     * @param string $to   with format 2020-01-05 19:10:05
     */
    
    
    public function importFromTo(string $from, string $to)
    {
        
        $this->setBoundariesFromTo($from, $to);
        $this->handle();
        
    }
    

    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Request data from Kiwi Grid...".PHP_EOL;
        echo "fetched data".PHP_EOL;
                      
        
        $client = new \GuzzleHttp\Client();
        
        $url = self::OAUTHENDPOINT;
        
        
        $token_request = [  'form_params' => [
            'client_id'     => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'channel'       => self::CHANNEL
        ],
            'headers'       => ['Accept' => 'application/json']
        ];
        
        //$request = new Request('POST', $url, $token_request);
        
        $response      = $client->request('POST', $url, $token_request);
        $response_json = $response->getBody()->getContents();
        $response_obj  = json_decode($response_json);
        
        $url = self::AUTHENTICATORENDPOINT."/".self::USER;
        
        $token_request = [  'form_params' => [
            'client_id'     => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'channel'       => self::CHANNEL
        ],
            'headers'       => ['Accept' => 'application/json', 'Authorization' => 'bearer '.$response_obj->access_token]
        ];
        
        
        $response     = $client->request('GET', $url, $token_request);
        $user_token   = json_decode($response->getBody()->getContents());    // just a string
        
        $token_request = [  'form_params' => [
            'client_id'     => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'channel'       => self::CHANNEL
        ],
            'headers'       => ['Accept' => 'application/json', 'Authorization' => 'bearer '.$user_token]
        ];
       
       
        $url = self::TIMESERIESREADSERVICEENDPOINT."/".self::DEVICE_GUID."/boundaries/".self::TAG_NAME;
        $response       = $client->request('GET', $url, $token_request);
        $response_json  = json_decode($response->getBody()->getContents());
        
        
        $dt_from = new \DateTime("@$response_json->from");
        $dt_to   = new \DateTime("@$response_json->to");
        
        $from = $dt_from->format("Y-m-d H:i:s");
        $to   = $dt_to->format("Y-m-d H:i:s");
        
        if (isset($this->console))
        $this->console->line("<fg=default;bg=black>Boundaries: <fg=green;bg=black> $from <fg=default;bg=black> to <fg=green;bg=black> $to <fg=default;bg=black></>");
        
        
        //$TIMESERIESREADSERVICEENDPOINT/$DEVICE_GUID/tagvalueatorbefore/$TAG_NAME?timestamp=$TIMEBOUNDARY_END
        $url = self::TIMESERIESREADSERVICEENDPOINT."/".self::DEVICE_GUID."/tagvalueatorbefore/".self::TAG_NAME."?timestamp=".$this->timeboundary_end;
        $response       = $client->request('GET', $url, $token_request);
        $response_json  = json_decode($response->getBody()->getContents());
        
        $t_stamp   = new \DateTime("@$response_json->timestamp");
        
        $stamp = $t_stamp->format("Y-m-d H:i:s");
        
        if (isset($this->console))
        $this->console->line("<fg=default;bg=black>Tag Value for Tagname: <fg=green;bg=black> $response_json->tagName <fg=default;bg=black> with Value <fg=green;bg=black> $response_json->value  <fg=default;bg=black> and timestamp  <fg=green;bg=black>  $stamp <fg=default;bg=black></>");
        
        // $TIMESERIESREADSERVICEENDPOINT/$DEVICE_GUID/aggregation/$TAG_NAME?from=$TIMEBOUNDARY_FROM\&to=$TIMEBOUNDARY_END\&config=$AGGREGATION_CONFIG_URI
        
        $config_url_encoded = rawurlencode(json_encode($this->aggregation_config));
        $url = self::TIMESERIESREADSERVICEENDPOINT."/".self::DEVICE_GUID."/aggregation/".self::TAG_NAME."?from=".$this->timeboundary_from."&to=".$this->timeboundary_end."&config=".$config_url_encoded;
        
        
        $response       = $client->request('GET', $url, $token_request);
        $response_json  = json_decode($response->getBody()->getContents());
        
        
        foreach($response_json as $line => $item)
        {
            $samples_processed = $item->meta->samplesProcessed;
            $samples_total     = $item->meta->samplesTotal;
            $from              = $item->from;
            $to                = $item->to;
            $value             = $item->value;
            
            
            $dt_from = new \DateTime("@$from");
            $dt_to   = new \DateTime("@$to");
            
            $from =  $dt_from->format("Y-m-d H:i:s");
            $to   =  $dt_to->format("Y-m-d H:i:s");
   
            if (isset($this->console)){
                $this->console->line("</><fg=default;bg=black>---====== Sample No <fg=green;bg=black> $line <fg=default;bg=black>-----=====</>");
                $this->console->line("<fg=default;bg=black>Total Samples:  <fg=green;bg=black> $samples_total <fg=default;bg=black> Processed: <fg=green;bg=black> $samples_processed <fg=default;bg=black></>");
                $this->console->line("<fg=default;bg=black>Date from: <fg=green;bg=black> $from <fg=default;bg=black></>");
                $this->console->line("<fg=default;bg=black>Date to: <fg=green;bg=black> $to <fg=default;bg=black></>");
                $this->console->line("<fg=default;bg=black>Value: <fg=green;bg=black> $value <fg=default;bg=black></>");
            }

        }
        
    }
    
    
    
}

