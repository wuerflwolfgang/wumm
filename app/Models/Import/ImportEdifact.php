<?php

namespace App\Models\Import;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use EDI\Parser;
use EDI\Interpreter;
use EDI\Analyser;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;


class ImportEdifact 
{
    
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    
    protected $importPath = '/app/import/edifact/pending/';
   

    /*
     * IEdifactImportPlugin 
     */
    protected $driver = null;
    
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edifact Importer';
    
    
    
    //
    public function __construct(\Illuminate\Console\Command $console=null)
    {
        if ($console)
        {
            $this->console = $console;
        }
    }
    
    public function setImportPath(string $path)
    {
       if (substr($path, -1) != '/') $path .= '/';
       $this->importPath = $path;    
    }
    
    public function getImportPath()
    {
        return $this->importPath;
    }
    
    
    public function importAll()
    {
        $path = storage_path().$this->importPath;
        foreach (glob($path."*.txt") as $filename) 
        {
            
            $size = filesize($filename);
            if (isset($this->console))
            {
                $this->console->line("<fg=default;bg=black></><fg=green;bg=black>Importing filename <fg=default;bg=black> $filename <fg=green;bg=black> - Size <fg=default;bg=black> $size ");
            }
            Log::info("Importing $filename - Size: ".filesize($filename));
            
            // Read file data and analyze message type
            $data   = file_get_contents($filename);
            $parser = new Parser();
            
            
            /* DEBUG 
            $analyser       = new Analyser();
            $prepped        = $analyser->process($parser->load($filename), $parser->getRawSegments());
            var_dump($prepped);continue;
            */
            $message_dir    = $parser->getMessageDirectory();
            $message_format = $parser->getMessageFormat();
            $message_data   = $parser->get();
           
            $reader         = new Reader($data);
            $groups         = $reader->groupsExtract('BGM');
            
            // Check if Multi-Message UNH>1+
            if (count($groups)>1) {
                
                $this->console->line("<fg=yellow;bg=black>Found Multi-Type Message <fg=default;bg=black>");
                $reader->setParsedFile($groups[1]);
            }
            $type           = $reader->readEdiDataValue('BGM',1);
            $ns             = "\\App\\Models\\Import\\Plugins\\".$type;
                   
            Log::info("Loading Plugins $ns");
            if ($this->console)
            {
                $this->console->line("<fg=default;bg=black></><fg=green;bg=black>Loading Plugin <fg=default;bg=black> $ns <fg=default;bg=black>");
            }
            $this->driver          = new $ns();
            $this->driver->console =  $this->console;
           
            $records = $this->driver->processData($filename);
                 
        }
    }
    

   
    
    
    
    
    
}
