<?php
namespace App\Models\Import\Plugins;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use EDI\Parser;
use Faker\Factory;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;
use App\Models\Import\Plugins\IEdifactImportPlugin;

use EDI\Encoder;
use App\Models\Export\ExportEdifact;
use App\Models\Export\Plugins\ControlMessage;

class Z16 implements IEdifactImportPlugin
{
    // 1-1 1 steht für Elektrizität -2 für gas
    // Medium - Kanal : Meßgröße . Meßart. Tarifstufe * Vorwertzählerstand
    protected $obis_type_codes =
    [
        '1-1:1.29.0' => ['Wirkarbeit Bezug(+)','Lastgang','total'],
        '1-1:9.99.0' => ['Wirkarbeit Bezug(+)','Lastgang','total'],
    ];
       
    
    
    // Z16 Profilschar
    
    protected $melo     = null;
    protected $receiver = null;
    protected $sender   = null;
    protected $messeinheit = null;
    protected $belegnr  = null;
    
    protected $exportPath = '/app/import/edifact/processed/';
    
    const UNIT = 16;
    
    public function processData(string $filename)
    {
        //echo "SKIP - TUNIX, not implemented!".PHP_EOL;
        
        $data = file_get_contents($filename);
        $parser = new Parser($data);
        
        $message_dir    = $parser->getMessageDirectory();
        $message_format = $parser->getMessageFormat();
        $message_data   = $parser->get();
        

        
        //<fg=green;bg=black>Green
        if (isset($this->console))
        {
            $this->console->line('<fg=default;bg=black>Importing Edifact Data</>');
            $this->console->line('<fg=default;bg=black>Source : File</>');
        }
        
        $records = []; // Array for all records
        $reader    = new Reader($data);
        //$r->setParsedFile($c->get());
        
        
        $groups = $reader->groupsExtract();
        
        
        
        foreach($groups as $item)
        {
            
            // Get first array from group, first array, first entry!!
            
            $type      = $item[0][0];
            $sequence  = $item[0][1]; // seq number fortl. Nummer
            
            $pia       = $item[1][0];
            $pia_num   = $item[1][1]; // seq number fortl. Nummer
            $pia_code  = $item[1][2][0]; // seq number fortl. Nummer
            $pia_code2 = $item[1][2][1]; // seq number fortl. Nummer
            
            $data1    = "";
            $data2    = "";
            
            if (isset($pia_code))
            {
                $this->messeinheit  = $pia_code;
            }
            
            if (isset($pia_code2))
            {
                $data2   = $pia_code2;
            }
            
            
            $date_beg = "";
            $date_end = "";
            $amount   = "";
            $unit     = "";
            $skipper  = false;
            
            foreach($item as $_item)
            {
               
   
                $_type  = $_item[0];
      
                
                if (strtoupper($_type) == 'PIA' || strtoupper($_type) == 'LIN')
                {
                    if (!$skipper)
                    {
                        $this->console->line("<fg=red;bg=black>New LIN Group  <fg=default;bg=black>");
                        $skipper = true;
                    }
                    continue;    
                }
                $skipper = false;
                
                $mode   = $_item[1][0];
                
                
                
                if (strtoupper($_type) == 'QTY')
                {
                    
                 
                        $amount   = $_item[1][1];
                        $unit     = $_item[1][0];

                }
                if (isset($this->console))
                    $this->console->line("<fg=green;bg=black>Type: <fg=default;bg=black>$type <fg=green;bg=black> Sequence No. <fg=default;bg=black>$sequence <fg=green;bg=black> Data1 <fg=default;bg=black> $data1 <fg=green;bg=black> Data2 <fg=default;bg=black>$data2 <fg=green;bg=black><fg=default;bg=black> $amount/$unit");
                
            }
               
                        
                        $amount = str_replace(",", ".", $amount);
                        $item = [   'amount'    => $amount,
                            'von'       => Carbon::now('Europe/Berlin'),
                            'bis'       => Carbon::now('Europe/Berlin'),
                            //'ctrluser'  => basename($filename),
                            
                        ];
                        
                        $records[] = $item;
                        
        }
        
        $sender    = $reader->readEdiDataValue('UNB', 2);
        
        $mess_type = $reader->readUNHmessageType();
        
        $unh       = $reader->readEdiDataValue('UNH', 2);
        $unh_data  = $reader->readUNHmessageNumber();
        $unh_msg_t = $reader->readUNHmessageType();
        $bgm       = $reader->readEdiDataValue('BGM', 1);
        $docnr     = $reader->readEdiDataValue('BGM', 3);
        
        $this->belegnr = $belegnr   = $reader->readEdiDataValue('BGM', 2);
       
        
        //$test      = $reader->readEdiDataValue('DTM', 4, 1);
        if (isset($this->console))
        {
            echo "BGM - Message Type ".$bgm.PHP_EOL;
            echo "BGM - Message Nr. /Belegnummer ".$belegnr.PHP_EOL;
            
            echo "Message type ".$mess_type.PHP_EOL;
            echo "Message code ".$unh[2].PHP_EOL;
            echo "Message code2 ".$unh[3].PHP_EOL;
            echo "Message version ".$unh[4].PHP_EOL;
        }
        
        
        
        
        $loc        = $reader->readEdiDataValue('LOC', 2);
        $this->melo = $loc;
        
       
        if (isset($this->console)) {
            echo "Loc:  " .$loc.PHP_EOL;
          
        }
        
       
        
        
        
        $nad = $reader->readGroups("RFF", "NAD", "NAD", "UNS"); // NAD eingeschlossen von RFF und UNS
        $sender     = "";
        $receiver   = "";
        $sendercode = "";
        $rec_code   = "";
        // get Sender/Receiver Information
        
       
        foreach($nad[0] as $item)
        {
            
            if (isset($item[0]))
            {
                
                if ($item[1] == 'MS') // sender
                {
                    
                    $sender     = $item[2][0];
                    $sendercode = $item[2][2];
                }
                
            }
            
            if (isset($item[0]))
            {
                
                if ($item[1] == 'MR') // sender
                {
                    
                    $receiver = $item[2][0];
                    $rec_code = $item[2][2];
                }
                
            }
        }
        
        if (isset($this->console)) echo "Receiver/Empfänger ".$receiver. " code ".$rec_code.PHP_EOL;
        $this->receiver = $receiver;
        if (isset($this->console)) echo "Sender " .$sender. " code ".$sendercode.PHP_EOL;
        $this->sender   = $sender;
        
        
        if (isset($this->console)) echo "Persisting to DB...";
        
        $this->persistData($records, $filename);
        
       
    }
    
    
    /**
     *
     * @param array $records
     * @param string $filename optional filename for file the data is associated
     */
    protected function persistData(array $records, $filename = null)
    {
        // Start Transaction
        DB::beginTransaction();
        try {
            $messstelle = Messstelle::where('name', $this->melo)->first();
            if (!$messstelle)
            {
                $messstelle        = new Messstelle();
                $messstelle->name  = $this->melo;
                $messstelle->descr = "descr for : ".$this->melo;
                $messstelle->save();
            }
            
            $messeinheit = Messeinheit::where('name', $this->messeinheit)->first();
            if (!$messeinheit)
            {
                $messeinheit        = new Messeinheit();
                $messeinheit->name  = $this->messeinheit;
                $messeinheit->descr = "Zählernummer";
            }
            
            $bezugseinheit = Bezugseinheit::where('name', $this->receiver)->first();
            
            
            if (!$bezugseinheit)
            {
                $bezugseinheit        = new Bezugseinheit();
                $bezugseinheit->name  = $this->receiver;
                $bezugseinheit->descr = "Bezugseinheit: ".$this->receiver;
                $bezugseinheit->save();
                
                $faker = Factory::create();
              
                
                $person               = Person::create([
                    'company_name'                   => $faker->unique()->company(),
                    'first_name'                     => $faker->firstName(),
                    'bezugseinheit_id'               => $bezugseinheit->id,
                    'last_name'                      => $faker->lastName(),
                    'email'                          => $faker->unique()->email(),
                    'street'                         => $faker->streetName()." ".$faker->streetSuffix(),
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'type'                           => 1,
                    'zip'                            => "98574",
                    'city'                           => "Schmalkalden",
                    
                ]);
                
                
                $bezugseinheit->person()->save($person);
            }
            
            // Create Summary Entry for data items
            $data_summary           = new MessungDataSummary();
            $data_summary->descr    = 'test';
            $data_summary->filename = basename($filename);
            $data_summary->save();
            
            $total_amount_per_period = null;
            foreach($records as &$record)
            {
                $total_amount_per_period    += (float)$record['amount'];
                $record['summary_id']       = $data_summary->id;
                $record['unit']             = self::UNIT;
            }
            
            $data_summary->sum = $total_amount_per_period;
            $data_summary->save();
            
            
            $messstelle->messeinheit()->save($messeinheit);
            $messeinheit->messung_data()->createMany($records);
            
            // n:m bezugseinheit zu messeinheit
            if (empty($bezugseinheit->messeinheit()->find($messeinheit->id))  )
            {
                $bezugseinheit->messeinheit()->save($messeinheit);
            }
            $count = count($records);
            $date  = date("Y-m-d");
            
            // create folder for date if not exist
            if (!file_exists(storage_path().$this->exportPath.$date)) 
            {
                mkdir(storage_path().$this->exportPath.$date, 0777, false);
            }
            
            // last step - Move File!!
            $_export_path = storage_path().$this->exportPath.$date."/".basename($filename);
            $_move_stat   = rename($filename, $_export_path);
            
            $this->createControlMessage();
            // if not already in exception mode, then explicit throw new exception!
            if (!$_move_stat) throw new \Exception("Couldn't move $filename to $_export_path");
            
        }catch(\Exception $ex)
        {
            DB::rollBack();
            if (isset($this->console))
            {
                echo "aborted".PHP_EOL;
                $this->console->line("<fg=default;bg=black></><fg=red;bg=black>Error Importing, Rollback!! <fg=default;bg=black>");
            }
            Log::error($ex->getMessage());
            
            return;
        }
        if (isset($this->console))
        {
            echo "success".PHP_EOL;
            $this->console->line("<fg=default;bg=black></><fg=green;bg=black>Imported <fg=default;bg=black>$count <fg=green;bg=black> datasets <fg=default;bg=black>");
        }
        Log::info("Succesfully imported $count datasets for $bezugseinheit->name $bezugseinheit->descr");
        
        DB::commit();
        
    }
    
    public function createControlMessage($filename = null)
    {
        
        // Empfänger_Sender_Datum_MessageID
        //$filename = "CONTRL__9911921000006_9900259000002_20200228_1116641.txt";
        
        $filename_template = "CONTRL__%s_%s_%s_%s.txt";
        $date =  date('y') . date('m') . date('d');
        //$ref  = '123456789';
        
        
        
        echo "Create Control Message...".PHP_EOL;
        
        
        /*
         $exporter     = new ExportEdifact();
         $data['type'] = 'Z06';
         $exporter->processData($data);
         
         $path     = storage_path().$importer->getImportPath();
         */
        
        $control_message = new ControlMessage();
        
        $control_message->setSenderDate();
        $control_message->setSender($this->sender);
        $control_message->setReceiver($this->receiver);
        $control_message->setMessageReference($this->belegnr);
        $ref = $control_message->setUNBId();
        $control_message->setUNHId();
        
        $output = $control_message->exportData([]);
        $c      = new Encoder();
        $c->enableUNA();
        $c->encode($output); // create edi string without una
        $test   = $c->get(); // prepend UNA and get created string
        
        $filename = sprintf($filename_template,$this->receiver, $this->sender, $date, $ref);
        echo "Write File to $filename".PHP_EOL;
        $control_message->saveFile($test, $filename);
        
    }
    
    
}

