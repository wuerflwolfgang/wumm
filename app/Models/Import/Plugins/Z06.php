<?php
namespace App\Models\Import\Plugins;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use EDI\Parser;
use EDI\Encoder;
use Faker\Factory;
use App\Models\Messeinheit;
use App\Models\Messstelle;
use App\Models\Person;
use App\Models\Import\Reader;
use App\Models\Bezugseinheit;
use App\Models\MessungDataSummary;
use App\Models\Export\ExportEdifact;
use App\Models\Export\Plugins\ControlMessage;



class Z06 implements IEdifactImportPlugin
{
    
    // 1-1 1 steht für Elektrizität -2 für gas
    // Medium - Kanal : Meßgröße . Meßart. Tarifstufe * Vorwertzählerstand
    protected $obis_type_codes =
    [
        '1-1:1.29.0' => ['Wirkarbeit Bezug(+)','Lastgang','total'],
        '1-1:9.99.0' => ['Wirkarbeit Bezug(+)','Lastgang','total'],
    ];
    
    
    // Z06 Fernauslesung
    // Z06 normiertes Profil
    // Z18 MALO
    // Z20 Abrechnunsintervall
    // Z22 Netznutzungsabrechnungsintervall
    // Z11 Zahlung des Netzzugangs
    protected $melo     = null;
    protected $receiver = null;
    protected $sender   = null;
    protected $messeinheit = null;
    protected $status      = true;
    protected $obis_code   = null;
    protected $belegnr     = null;
    
    protected $exportPath = '/app/import/edifact/processed/';
    
    const UNIT = 6;
    

    
    
    
    
    public function processData(string $filename)
    {

        $data = file_get_contents($filename);

        //<fg=green;bg=black>Green
        if (isset($this->console))
        {
            $this->console->line('<fg=default;bg=black>Importing Edifact Data</>');
            $this->console->line('<fg=default;bg=black>Source : File</>');
        }
        
        $records = []; // Array for all records
        $reader    = new Reader($data);
        //$r->setParsedFile($c->get());
          
        $groups = $reader->groupsExtract('UNH', ['UNT']);
        //var_dump($groups[0]);die;
        // process each group!!!
        DB::beginTransaction();
        
        foreach($groups as $group)
        {
            $reader->setParsedFile($group);
            $group         = $reader->groupsExtract('QTY');
            foreach($group as $item)
            {
                

                // Get first array from group, first array, first entry!!
               
                $qty      = 
                $begin    = null;
                $end      = null;
                
                //$sequence = $item[0][1]; // seq number fortl. Nummer

                
                $firstrun = true;
                $date_beg = "";
                $date_end = "";
                $amount   = "";
                $unit     = "";
                foreach($item as $_item)
                {
                    
                    
                    $_type  = $_item[0];
                    $mode   = $_item[1][0];
                    
                    
                    if ($mode == 163 && $_item[0] == 'DTM')
                    {
                        $date_beg = $_item[1][1];
                        $date_beg = \preg_replace('#(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)#', '$1-$2-$3 $4:$5:00', $date_beg);
                    }
                    
                    if ($mode == 164 && $_item[0] == 'DTM')
                    {
                        $date_end = $_item[1][1];
                        $date_end = \preg_replace('#(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)#', '$1-$2-$3 $4:$5:00', $date_end);
                    }
                    
                    if ($_type == 'QTY')
                    {
                            $amount = $_item[1][1];
                    }
                    
                }
                if (isset($this->console))
                    $this->console->line("<fg=green;bg=black>Type: <fg=default;bg=black>$_type <fg=green;bg=black> Date Begin <fg=default;bg=black> $date_beg <fg=green;bg=black>Date End <fg=default;bg=black> $date_end <fg=green;bg=black>Amount <fg=default;bg=black> $amount");
                    
                    
                    $split_time = explode('+', $date_beg);
                    $von        = $split_time[0]; // Zeit
                    if (isset($split_time[1]))
                        $von_offset = (int)$split_time[1]; // Offset Sommer/Winter in Std
                        
                        
                        $split_time = explode('+', $date_end);
                        $bis        = $split_time[0]; // Zeit
                        if (isset($split_time[1]))
                            $bis_offset = (int)$split_time[1]; // Offset Sommer/Winter in Std
                            
                            $amount = str_replace(",", ".", $amount);
                            $item = [   'amount'    => $amount,
                                'von'       => $von,
                                'bis'       => $bis,
                                //'ctrluser'  => basename($filename),
                                
                            ];
                            
                            $records[] = $item;
                            
            }
            
            $sender    = $reader->readEdiDataValue('UNB', 2);
            
            
            $mess_type = $reader->readUNHmessageType();
            
            $unh       = $reader->readEdiDataValue('UNH', 2);
            $unh_data  = $reader->readUNHmessageNumber();
            $unh_msg_t = $reader->readUNHmessageType();
            $bgm       = $reader->readEdiDataValue('BGM', 1);
            $belegnr   = explode("-",$reader->readEdiDataValue('BGM', 2));
            $this->belegnr = $belegnr[0]; 
            
            //$test      = $reader->readEdiDataValue('DTM', 4, 1);
            if (isset($this->console))
            {
                echo "BGM - Code Market location ".$bgm.PHP_EOL;
                echo "BGM - Message Nr. /Belegnummer ".$this->belegnr.PHP_EOL;
                
                echo "Message type ".$mess_type.PHP_EOL;
                echo "Message code ".$unh[2].PHP_EOL;
                echo "Message code2 ".$unh[3].PHP_EOL;
                echo "Message version ".$unh[4].PHP_EOL;
            }
            
            
            
            
         
            $loc        = $reader->readEdiDataValue('LOC', 2);
          
            $this->melo = $loc;
            
            if (isset($this->console)) {
            
                echo "Loc Code: ".$loc.PHP_EOL;
            }
            
            $pia_code       = $reader->readEdiDataValue('PIA', 2);
            
            if (isset($pia_code[0]))
            {
                $this->messeinheit  = $pia_code[0];
                $this->obis_code    = $pia_code[0]; 
            }
            
            
            /*
             * RFF Prüfidentifikator Z13!!
             */
            
            
            $nad = $reader->readGroups("RFF", "NAD", "NAD", "UNS"); // NAD eingeschlossen von RFF und UNS
            $sender     = "";
            $receiver   = "";
            $sendercode = "";
            $rec_code   = "";
            // get Sender/Receiver Information
            
            
            foreach($nad[0] as $item)
            {
                
                if (isset($item[0]))
                {
                    
                    if ($item[1] == 'MS') // sender
                    {
                        $sender     = $item[2][0];
                        $sendercode = $item[2][2];
                    }
                    
                }
                
                if (isset($item[0]))
                {
                    
                    if ($item[1] == 'MR') // sender
                    {
                        
                        $receiver = $item[2][0];
                        $rec_code = $item[2][2];
                    }
                    
                }
            }
            
            if (isset($this->console)) echo "Receiver/Empfänger ".$receiver. " code ".$rec_code.PHP_EOL;
            $this->receiver = $receiver;
            if (isset($this->console)) echo "Sender " .$sender. " code ".$sendercode.PHP_EOL;
            $this->sender   = $sender;
            
            
            if (isset($this->console)) echo "Persisting to DB...";
            
            $this->persistData($records, $filename);
            
           // End outer loop - group UNH+ 
            
            
        }

        if ($this->status)
        {
            try 
            {
                $count = count($records);
                $date  = date("Y-m-d");
                
                // create folder for date if not exist
                if (!file_exists(storage_path().$this->exportPath.$date)) {
                    
                    mkdir(storage_path().$this->exportPath.$date, 0777, false);
                }
                
                // last step - Move File!!
                $_export_path = storage_path().$this->exportPath.$date."/".basename($filename);
                $_move_stat   = rename($filename, $_export_path);
                // Create a Control Message
                $this->createControlMessage();
                // if not already in exception mode, then explicit throw new exception!
                if (!$_move_stat) throw new \Exception("Couldn't move $filename to $_export_path");
                

                
            }catch (\Exception $ex)
            {
                if (isset($this->console))
                    $this->console->line("<fg=red;bg=black>Something went wrong by moving to path: <fg=default;bg=black>$_export_path <fg=default;bg=black>");
                    
                Log::info("Something went wrong by moving to export path $_export_path");
                Log::debug($ex);
                DB::rollBack();
            }
            
            DB::commit();
            Log::info("Succesfully imported $count datasets for xxx");
        }
        


        
        //var_dump($reader->groupsExtract());
    }
    
    /**
    *
    * @param array $records
    * @param string $filename optional filename for file the data is associated
    */
    protected function persistData(array $records, $filename = null)
    {
        // Start Transaction
       
        try {
            $messstelle = Messstelle::where('name', $this->melo)->first();
            if (!$messstelle)
            {
                $messstelle        = new Messstelle();
                $messstelle->name  = $this->melo;
                $messstelle->descr = "descr for : ".$this->melo;
                $messstelle->save();
            }
            
            $messeinheit = Messeinheit::where('name', $this->messeinheit)->first();
            if (!$messeinheit)
            {
                $messeinheit        = new Messeinheit();
                $messeinheit->name  = $this->messeinheit;
                $messeinheit->descr = "Zählernummer";
            }
            
            $bezugseinheit = Bezugseinheit::where('name', $this->receiver)->first();
            if (!$bezugseinheit)
            {
                $bezugseinheit        = new Bezugseinheit();
                $bezugseinheit->name  = $this->receiver;
                $bezugseinheit->descr = "Bezugseinheit: ".$this->receiver;
                $bezugseinheit->save();
                
                $faker = Factory::create();
                
                
                $person               = Person::create([
                    
                    'company_name'                   => $faker->unique()->company(),
                    'first_name'                     => $faker->firstName(),
                    'bezugseinheit_id'               => $bezugseinheit->id,
                    'last_name'                      => $faker->lastName(),
                    'email'                          => $faker->unique()->email(),
                    'street'                         => $faker->streetName()." ".$faker->streetSuffix(),
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'type'                           => 1,
                    'zip'                            => "98574",
                    'city'                           => "Schmalkalden",
                    
                ]);
                
                
                $bezugseinheit->person()->save($person);
            }
            
            // Create Summary Entry for data items
            $data_summary           = new MessungDataSummary();
            $data_summary->descr    = 'test';
            $data_summary->filename = basename($filename);
            $data_summary->save();
            
            $total_amount_per_period = null;
            foreach($records as &$record)
            {
                $total_amount_per_period    += (float)$record['amount'];
                $record['summary_id']       = $data_summary->id;
                $record['unit']             = self::UNIT;
            }
            
            $data_summary->sum = $total_amount_per_period;
            $data_summary->save();
            
            
            $messstelle->messeinheit()->save($messeinheit);
            $messeinheit->messung_data()->createMany($records);
            
            // n:m bezugseinheit zu messeinheit
            if (empty($bezugseinheit->messeinheit()->find($messeinheit->id))  )
            {
                $bezugseinheit->messeinheit()->save($messeinheit);
            }


        }catch(\Exception $ex)
        {
            $this->status = false;
            
            DB::rollBack();
            if (isset($this->console))
            {
                echo "aborted".PHP_EOL;
                $this->console->line("<fg=default;bg=black></><fg=red;bg=black>Error Importing, Rollback!! <fg=default;bg=black>");
            }
            Log::error($ex->getMessage());
            
            return;
        }
        if (isset($this->console))
        {
            echo "success".PHP_EOL;
            $count = count($records);
            $this->console->line("<fg=default;bg=black></><fg=green;bg=black>Imported <fg=default;bg=black>$count <fg=green;bg=black> datasets <fg=default;bg=black>");
        }
        
      
        
    }
    
    
    /**
     * Create Control Message for Acknowledge
     * @param unknown $filename
     */
    
    public function createControlMessage($_filename = null)
    {
        // Empfänger_Sender_Datum_MessageID
        //$filename = "CONTRL__9911921000006_9900259000002_20200228_1116641.txt";
        
        $filename_template = "CONTRL__%s_%s_%s_%s.txt";
        $date =  date('y') . date('m') . date('d');
        //$ref  = '123456789';
        
     
        
        echo "Create Control Message...".PHP_EOL;
        
       
        /*
        $exporter     = new ExportEdifact();
        $data['type'] = 'Z06';
        $exporter->processData($data);
                
        $path     = storage_path().$importer->getImportPath();
        */
        
        $control_message = new ControlMessage();
        
        $control_message->setSenderDate();
        $control_message->setSender($this->sender);
        $control_message->setReceiver($this->receiver);
        $control_message->setMessageReference($this->belegnr);
        $ref = $control_message->setUNBId();
        $control_message->setUNHId();
        
        $output = $control_message->exportData([]);
        $c      = new Encoder();
        $c->enableUNA();
        $c->encode($output); // create edi string without una
        $test   = $c->get(); // prepend UNA and get created string
        
        $filename = sprintf($filename_template,$this->receiver, $this->sender, $date, $ref);
        echo "Write File to $filename".PHP_EOL;
        $control_message->saveFile($test, $filename);
        
    }
    
    
    
}

