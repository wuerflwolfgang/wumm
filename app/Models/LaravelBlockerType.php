<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LaravelBlockerType
 * 
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|LaravelBlocker[] $laravel_blockers
 *
 * @package App\Models
 */
class LaravelBlockerType extends Model
{
	use SoftDeletes;
	protected $table = 'laravel_blocker_types';

	protected $fillable = [
		'slug',
		'name'
	];

	public function laravel_blockers()
	{
		return $this->hasMany(LaravelBlocker::class, 'typeId');
	}
}
