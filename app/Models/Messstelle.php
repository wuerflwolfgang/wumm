<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Messstelle
 * 
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descr
 * @property string $ctrluser
 * @property Carbon $ctrldate
 * @property string $ctrlstate
 * 
 * @property Collection|Messeinheit[] $messeinheit
 *
 * @package App\Models
 */
class Messstelle extends Model
{
	protected $table = 'messstelle';

	protected $dates = [
		'ctrldate'
	];

	protected $fillable = [
		'descr',
		'ctrluser',
		'ctrldate',
		'ctrlstate'
	];

	public function messeinheit()
	{
		return $this->hasMany(Messeinheit::class);
	}
}
