<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Bezugseinheit;
use App\Models\MessungData;
use App\Http\Requests\DeleteBezugseinheit;
use App\Traits\CaptureIpTrait;

class BezugseinheitController extends Controller
{
    
   
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination');
        if ($paginationEnabled) {
            $bezugseinheiten = Bezugseinheit::paginate(config('usersmanagement.paginateListSize'));
        } else {
            $bezugsheinheiten = Bezugseinheit::all();
        }
        //$roles = Role::all();
        
        return View('wumm.bezugseinheit', compact('bezugseinheiten'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\DeleteBezugseinheit $request
     * @param int                                  $id
     *
     * @return \Illuminate\Http\Response
     */
    
    public function deleteBezugseinheit(DeleteBezugseinheit $request, $id)
    {
        
         $bezugseinheit = Bezugseinheit::findOrFail($id);
         $ipAddress     = new CaptureIpTrait();

        
        // Save Restore Token and Ip Address
        // $user->token = $level5;
        $_addr = $ipAddress->getClientIp();
        Log::info("Bezugseinheit with id $bezugseinheit->name has been deleted by ip $_addr");      
        // Send Goodbye email notification
        //$this->sendGoodbyEmail($user, $user->token);
        
        // Soft Delete User
        $status  = 'success';
        $message = 'successBezugseinheitDeleted';
        try {
            $bezugseinheit->delete();
        }catch (\Exception $ex)
        {
            $status  = 'error';
            $message = 'errorBezugseinheitDeleted';
        }
  
       
        return redirect('/bezugseinheit/')->with($status, trans('wumm.'.$message));
    }
    
    public function showBezugseinheit(DeleteBezugseinheit $request, $id)
    {
        
        $einheit        = Bezugseinheit::findOrFail($id);
        
        
        $paginationEnabled = config('usersmanagement.enablePagination', true);
        if ($paginationEnabled) {
            $messeinheiten = $einheit->messeinheit()->paginate(config('usersmanagement.paginateListSize'));
        }else {
            $messeinheiten = $einheit->messeinheit()->get();
        }

        Log::debug($messeinheiten);
        Log::debug("zugeordnete person");
        Log::debug($einheit->person()->get());
        Session::put("last_bezugseinheiten_id", $id);
        $levelAmount = "1";
        return View('wumm.show-user', compact('einheit', 'levelAmount', 'messeinheiten'));
        
    }
    
    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('user_search_box');
        $searchRules = [
            'user_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'user_search_box.required' => 'Search term is required',
            'user_search_box.string'   => 'Search term has invalid characters',
            'user_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);
        
        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        $results = Bezugseinheit::where('id', 'like', $searchTerm.'%')
        ->orWhere('name', 'like', $searchTerm.'%')
        ->orWhere('descr', 'like', $searchTerm.'%')
        ->orWhere('von', 'like', $searchTerm.'%')->get();
        
        // Attach roles to results
        /*
        
        foreach ($results as $result) {
        $roles = [
        'roles' => $result->roles,
        ];
        $result->push($roles);
        } */
        
        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
    
}