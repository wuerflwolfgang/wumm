<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\DeleteSummary;
use App\Models\MessungDataSummary;
use App\Traits\CaptureIpTrait;

class SummaryController extends Controller
{
    //
    
    
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination', true);
        if ($paginationEnabled) {
            $summary   = MessungDataSummary::paginate(config('usersmanagement.paginateListSize'));
        }else {
            $summary   = MessungDataSummary::all();
        }
        
        return View('wumm.summary', compact('summary'));
    }
    
    
    public function deleteSummary(DeleteSummary $request, $id)
    {
        
        $summary = MessungDataSummary::findOrFail($id);
        $ipAddress     = new CaptureIpTrait();
        
        
        // Save Restore Token and Ip Address
        // $user->token = $level5;
        $_addr = $ipAddress->getClientIp();
        Log::info("Summary with filename $summary->filename and id $summary->id has been deleted by ip $_addr");
        // Send Goodbye email notification
        //$this->sendGoodbyEmail($user, $user->token);
        
        // Soft Delete User
        $status  = 'success';
        $message = 'successSummaryDeleted';
        try {
            $summary->delete();
        }catch (\Exception $ex)
        {
            $status  = 'error';
            $message = 'errorSummaryDeleted';
        }
 
        return redirect('/summary')->with($status, trans('wumm.'.$message));
    }
    
    
}
