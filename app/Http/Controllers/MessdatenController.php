<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\MessungData;

class MessdatenController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination');
        if ($paginationEnabled) {
            $messdaten = MessungData::paginate(config('usersmanagement.paginateListSize'));
            
            
        } else {
            $messdaten = MessungData::all();
        }
        //$roles = Role::all();
        
       
        
        return View('wumm.messdaten', compact('messdaten'));
    }
    
    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('user_search_box');
        $searchRules = [
            'user_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'user_search_box.required' => 'Search term is required',
            'user_search_box.string'   => 'Search term has invalid characters',
            'user_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);
        
        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        $results = MessungData::where('id', 'like', $searchTerm.'%')
        ->orWhere('amount', 'like', $searchTerm.'%')
        ->orWhere('bis', 'like', $searchTerm.'%')
        ->orWhere('von', 'like', $searchTerm.'%')->get();
        
        // Attach roles to results
        /*
        
        foreach ($results as $result) {
        $roles = [
        'roles' => $result->roles,
        ];
        $result->push($roles);
        } */
        
        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
    
}
