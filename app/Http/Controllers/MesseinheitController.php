<?php

namespace App\Http\Controllers;

use App\Models\Messeinheit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\DeleteMesseinheit;
use App\Traits\CaptureIpTrait;


class MesseinheitController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function showMesseinheit(Request $request, $id)
    {

        $messeinheit = Messeinheit::findOrFail($id);
        $paginationEnabled = config('usersmanagement.enablePagination', true);
        if ($paginationEnabled) {
            $messdaten   = $messeinheit->messung_data()->paginate(config('usersmanagement.paginateListSize'));
        }else {
            $messdaten   = $messeinheit->messung_data()->get();
        }

        return View('wumm.messeinheit_details', compact('messeinheit','messdaten'));
        
    }
    
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination', true);
        if ($paginationEnabled) {
            $messeinheit   = Messeinheit::paginate(config('usersmanagement.paginateListSize'));
        }else {
            $messeinheit   = Messeinheit::all();
        }
       
        return View('wumm.messeinheit', compact('messeinheit'));
        
    }
    
    /**
     * delete the specified resource in storage.
     *
     * @param \App\Http\Requests\DeleteMesseinheit $request
     * @param int                                  $id
     *
     * @return \Illuminate\Http\Response
     */
    
    public function deleteMesseinheit(DeleteMesseinheit $request, $id)
    {
        
        $messeinheit = Messeinheit::findOrFail($id);
        $ipAddress     = new CaptureIpTrait();
        
        
        // Save Restore Token and Ip Address
        // $user->token = $level5;
        $_addr = $ipAddress->getClientIp();
        Log::info("Bezugseinheit with id $messeinheit->name has been deleted by ip $_addr");
        // Send Goodbye email notification
        //$this->sendGoodbyEmail($user, $user->token);
        
        // Soft Delete User
        $status  = 'success';
        $message = 'successBezugseinheitDeleted';
        try {
            $messeinheit->delete();
        }catch (\Exception $ex)
        {
            $status  = 'error';
            $message = 'errorBezugseinheitDeleted';
        }
        $bez = Session::get("last_bezugseinheiten_id", 1);
        return redirect('/messeinheit/'.$bez)->with($status, trans('wumm.'.$message));
    }
    
    
    
    public function deleteMesseinheitMain(DeleteMesseinheit $request, $id)
    {
        
        $messeinheit = Messeinheit::findOrFail($id);
        $ipAddress     = new CaptureIpTrait();
        
        
        // Save Restore Token and Ip Address
        // $user->token = $level5;
        $_addr = $ipAddress->getClientIp();
        Log::info("Bezugseinheit with id $messeinheit->name has been deleted by ip $_addr");
        // Send Goodbye email notification
        //$this->sendGoodbyEmail($user, $user->token);
        
        // Soft Delete User
        $status  = 'success';
        $message = 'successBezugseinheitDeleted';
        try {
            $messeinheit->delete();
        }catch (\Exception $ex)
        {
            $status  = 'error';
            $message = 'errorBezugseinheitDeleted';
        }
       
        return redirect('/messeinheit')->with($status, trans('wumm.'.$message));
    }
    
    
    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('user_search_box');
        $searchRules = [
            'user_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'user_search_box.required' => 'Search term is required',
            'user_search_box.string'   => 'Search term has invalid characters',
            'user_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);
        
        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        $results = Messeinheit::where('id', 'like', $searchTerm.'%')
        ->orWhere('name', 'like', $searchTerm.'%')
        ->orWhere('descr', 'like', $searchTerm.'%')
        ->orWhere('bis', 'like', $searchTerm.'%')
        ->orWhere('von', 'like', $searchTerm.'%')->get();
        
        // Attach roles to results
        /*
        
        foreach ($results as $result) {
        $roles = [
        'roles' => $result->roles,
        ];
        $result->push($roles);
        } */
        
        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
}
