<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\DeleteMessstelle;
use App\Models\Messstelle;
use App\Traits\CaptureIpTrait;

class MessstelleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination');
        if ($paginationEnabled) {
            $messstellen = Messstelle::paginate(config('usersmanagement.paginateListSize'));
        } else {
            $messstellen = Messstelle()::all();
        }
        //$roles = Role::all();
        
        return View('wumm.messstelle', compact('messstellen'));
    }
    
    
    /**
     * delete the specified resource in storage.
     *
     * @param \App\Http\Requests\DeleteMesseinheit $request
     * @param int                                  $id
     *
     * @return \Illuminate\Http\Response
     */
    
    public function deleteMessstelle(DeleteMessstelle $request, $id)
    {
        
        $messstelle = Messstelle::findOrFail($id);
        $ipAddress     = new CaptureIpTrait();
        
        
        // Save Restore Token and Ip Address
        // $user->token = $level5;
        $_addr = $ipAddress->getClientIp();
        Log::info("Messstelle with id $messstelle->id and Name $messstelle->name has been deleted by ip $_addr");
        // Send Goodbye email notification
        //$this->sendGoodbyEmail($user, $user->token);
        
        // Soft Delete User
        $status  = 'success';
        $message = 'successMessstelleDeleted';
        try {
            $messstelle->delete();
        }catch (\Exception $ex)
        {
            $status  = 'error';
            $message = 'errorMessstelleDeleted';
        }
        //$bez = Session::get("last_bezugseinheiten_id", 1);
        return redirect('/messstelle')->with($status, trans('wumm.'.$message));
    }
    
    
    
    
    
    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('user_search_box');
        $searchRules = [
            'user_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'user_search_box.required' => 'Search term is required',
            'user_search_box.string'   => 'Search term has invalid characters',
            'user_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);
        
        if ($validator->fails()) 
        {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        $results = Messstelle::where('id', 'like', $searchTerm.'%')
        ->orWhere('name', 'like', '%'.$searchTerm.'%')
        ->orWhere('descr', 'like', '%'.$searchTerm.'%')->get();
        
        // Attach roles to results
       /* 
        
        foreach ($results as $result) {
            $roles = [
                'roles' => $result->roles,
            ];
            $result->push($roles);
        } */
        
        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
    
}
