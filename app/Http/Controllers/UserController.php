<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Import\ImportEdifact;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            
            $_path         = (new ImportEdifact())->getImportPath();
            $files         = glob($_path);
            $pendingfiles = count($files);
            return view('pages.admin.home', compact('pendingfiles'));
        }

        return view('pages.user.home');
    }
}
