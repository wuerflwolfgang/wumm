<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Bezugseinheit;

use iio\libmergepdf\Merger;

class WummController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    
    
    public function testPDF()
    {
        
        //
        $data = ['title'   => 'TEST',
            'heading' => 'KOPF',
            'content' => 'content'
            
        ];
        $mail = 'udo.fischer@excello.eu';
        //$this->line("<fg=default;bg=black>Erstelle Rechnung für email:<fg=green;bg=black> $mail <fg=default;bg=black>");
        
        $person         = Person::find(2);
        $bezugseinheit  = $person->bezugseinheit()->first();
        
        

        $year = 2014;
        $month = 11;
        
        foreach($bezugseinheit->messeinheit()->get() as $messeinheit)
        {
            $anz_dataset           = count($messeinheit->messung_data()->whereYear('von', $year)->whereMonth('von', $month)->get());
         
 
            $data['messeinheit'][$messeinheit->name] = ['name' => $messeinheit->name,
                'data' => $messeinheit->messung_data()->whereYear('von', $year)->whereMonth('von', $month)->get()
            ];
            
        }
        

        $image = Image::make(\Storage::disk('public')->get('invoice/Logo_wumm.png'))->resize(320,148)->encode();
        \Storage::disk('public')->put('invoice/resized_wumm.png', $image);
        

        
        // Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()
        foreach ( $data['messeinheit'] as $messeinheit)
        {
            $messeinheit['path'] = Storage::disk('public')->url("invoice/resized_wumm.png");
            
            $sum                   = $messeinheit['data']->pluck('amount')->sum();
            $messeinheit['von']    = $messeinheit['data']->first()->von->format('d.m.Y');
            $messeinheit['bis']    = $messeinheit['data']->last()->bis->format('d.m.Y');
            $messeinheit['alt']    = 0;
            $messeinheit['unit']   = 'kw/h';
            
            $messeinheit['neu']    = $messeinheit['alt'] + $sum;
            $messeinheit['amount'] = $messeinheit['data']->pluck('amount')->sum();
            $messeinheit['person'] = $person;
            
            
            /*
            $pdf       = PDF::loadView('wumm.invoice', $messeinheit);
            $plain_pdf = $pdf->download('medium.pdf');
            \Storage::disk('public')->put('invoice/invoice.pdf', $plain_pdf);
            
          //  $invoice = \Storage::disk('public')->path('invoice/invoice.pdf');
           // $append  = \Storage::disk('public')->path('invoice/AnlageWumm.pdf');
            
          
            
            $merger = new Merger;
            $merger->addIterator([$invoice, $append]);
            $createdPdf = $merger->merge();
            
            \Storage::disk('public')->put('invoice/invoice_complete.pdf', $createdPdf);
            */
        }
        /*$merger = new Merger;
        $merger->addIterator([$invoice, $append]);
        $createdPdf = $merger->merge();
        \Storage::disk('public')->put('invoice/invoice_complete.pdf', $createdPdf);
        */
        return view('wumm.invoice', ['path' => $messeinheit['path'],
                                     'von'  => $messeinheit['von'],
                                     'bis'  => $messeinheit['bis'],
                                     'amount' => $messeinheit['amount'],
                                     'person' => $person,
                                     'alt'    => $messeinheit['alt'],
                                     'neu'    => $messeinheit['neu'],
                                     'unit'   => $messeinheit['unit'],
                                     'name'   => $messeinheit['name'],
                                     'data'   => $messeinheit['data']
            
        ]);
    }
}
