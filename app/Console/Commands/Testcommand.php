<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Messstelle;
use App\Models\Messeinheit;
use App\Models\MessungData;
use App\Models\Bezugseinheit;
use App\Models\Import\ImportEdifact as ImpEdi;
use App\Models\Export\ExportEdifact;
use EDI\Parser;
use EDI\Encoder;
use App\Models\Export\Plugins\ControlMessage;

class Testcommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Wumm:ExportTest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Importer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      
        echo "Encoding...".PHP_EOL;

        $exporter     = new ExportEdifact();
        $data['type'] = 'Z16';
        $exporter->processData($data);

        $importer = new ImpEdi($this);
        $path     = storage_path().$importer->getImportPath();
        
        $control_message = new ControlMessage();
        
       

        
        $control_message->setSenderDate();
        $control_message->setSender("12393");
        $control_message->setReceiver("987654321");
        $control_message->setMessageReference("N0377383");
        $control_message->setUNBId();
        $control_message->setUNHId();
        

     
        $output = $control_message->exportData([]);
        var_dump($output);
        
        $c      = new Encoder();
        $c->enableUNA();
        $c->encode($output); // create edi string without una
        $test   = $c->get(); // prepend UNA and get created string

        $control_message->saveFile($test, 'test.txt');
        echo $test;
        die;
        /*
        foreach (glob($path."*.txt") as $filename)
        {
       
            echo $filename.PHP_EOL;
            $data   = file_get_contents($filename);
            $parser = new Parser($data);
            var_dump($parser->errors());
            //var_dump($parser->get());die;
            $test = var_export($parser->get(), true);
            file_put_contents("test.arr", $test);
            
            $c    = new Encoder();
            $c->enableUNA();
            $c->encode($parser->get()); // create edi string without una 
            //var_dump($test);
            $test = $c->get(); // prepend UNA and get created string
            
           
            file_put_contents($filename.".generated", $test);
            
        }
            */
        echo "created".PHP_EOL;
        
        //
    }
}
