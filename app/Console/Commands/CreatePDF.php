<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Bezugseinheit;
use App\Models\Person;
use iio\libmergepdf\Merger;
use Symfony\Component\Process\Process;

class CreatePDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Wumm:CreatePDF {email} {--year=2019} {--month=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create PDF';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $data = ['title'   => 'TEST',
                 'heading' => 'KOPF',
                 'content' => 'content'
            
        ];
        $mail = 'udo.fischer@excello.eu';
      
        
        if ($this->argument('email'))
        {
            $mail = $this->argument('email');
           
        }
        
        $this->line("<fg=default;bg=black>Erstelle Rechnung für email:<fg=green;bg=black> $mail <fg=default;bg=black>");
        
        $person         = Person::where('email', $mail)->first();
        if (!$person)
        {
            $this->line("<fg=red;bg=black>Kann keinen Kunden für die angegebene Email finden : <fg=green;bg=black>$mail <fg=default;bg=black>");
            return;
        }
        
        
        $bar = $this->output->createProgressBar(3);

        
        $bezugseinheit  = $person->bezugseinheit()->first();
        
        $year  = $this->option('year');
        $month = $this->option('month');
        
        
        foreach($bezugseinheit->messeinheit()->get() as $messeinheit)
        {
            $anz_dataset           = count($messeinheit->messung_data()->whereYear('von', $year)->whereMonth('von', $month)->get());
            $this->line("<fg=default;bg=black>Lade Messwerte für Messeinheit/Zählernummer: <fg=green;bg=black>$messeinheit->name <fg=default;bg=black> with <fg=green;bg=black> $anz_dataset <fg=default;bg=black> datasets");
            $bar->start();
            $bar->advance();
            $data['messeinheit'][$messeinheit->name] = ['name' => $messeinheit->name, 
                                                        'data' => $messeinheit->messung_data()->whereYear('von', $year)->whereMonth('von', $month)->get()
                                     ];

        }


        $bar->advance();

        $image = Image::make(\Storage::disk('public')->get('invoice/Logo_wumm.png'))->resize(320,148)->encode();
        \Storage::disk('public')->put('invoice/resized_wumm.png', $image);
               
        // Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()
        foreach ( $data['messeinheit'] as $messeinheit)
        {
            $messeinheit['path'] = Storage::disk('public')->url("invoice/resized_wumm.png");
           
            $sum                   = $messeinheit['data']->pluck('amount')->sum();
            $messeinheit['von']    = $messeinheit['data']->first()->von->format('d.m.Y');
            $messeinheit['bis']    = $messeinheit['data']->last()->bis->format('d.m.Y');
            $messeinheit['alt']    = 0;
            $messeinheit['unit']   = 'kw/h';
            
            $messeinheit['neu']    = $messeinheit['alt'] + $sum;
            $messeinheit['amount'] = $messeinheit['data']->pluck('amount')->sum();
            $messeinheit['person'] = $person;
         
         
            //PDF::setOptions(['dpi' => 600, 'defaultFont' => 'sans-serif']);
            /*
            $pdf       = PDF::loadView('wumm.invoice', $messeinheit);
            $plain_pdf = $pdf->download('medium.pdf');
            \Storage::disk('public')->put('invoice/invoice.pdf', $plain_pdf);
            */
            
            $append  = \Storage::disk('public')->path('invoice/AnlageWumm.pdf');
            
            $html = view('wumm.invoice', $messeinheit)->render();
            $invoice = \Storage::disk('public')->path('invoice/invoice.html');
            $pdf     = \Storage::disk('public')->path('invoice/invoice.pdf');
            \Storage::disk('public')->put('invoice/invoice.html', $html);
            
            $process = new Process(["/usr/bin/wkhtmltopdf", $invoice, $pdf ]);
            $process->start();
            
            foreach ($process as $type => $data) 
            {
                if ($process::OUT === $type) 
                {
                   // echo "\nRead from stdout: ".$data;
                } else 
                { // $process::ERR === $type
                  //  echo "\nRead from stderr: ".$data;
                }
            }
            
            $bar->advance();
    
            $merger = new Merger;
            $merger->addIterator([$pdf, $append]);
            $createdPdf = $merger->merge();
            \Storage::disk('public')->put('invoice/invoice_complete.pdf', $createdPdf);
        }
        
        $bar->finish();
        
    }
}
