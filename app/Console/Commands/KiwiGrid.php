<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Psr7\Request;
use App\Models\Messstelle;
use App\Models\Messeinheit;
use App\Models\MessungData;
use App\Models\Bezugseinheit;
use App\Models\Import\ImportKiwiGrid;
use OpenAPI\Client\Api\TimeSeriesReadServiceApi;


class KiwiGrid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Wumm:KiwiGrid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiwi Grid API';
    
                                           
                                                                                

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * 
     * @param string $from with format 2020-01-05 19:10:05
     * @param string $to
     */
    public function setBoundariesFromTo(string $from, string $to)
    {
        $this->timeboundary_from =  \DateTime::createFromFormat("Y-m-d H:i:s", $from)->getTimestamp();
        $this->timeboundary_end  =  \DateTime::createFromFormat("Y-m-d H:i:s", $to)->getTimestamp();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
       /*     

        $config = new \OpenAPI\Client\Configuration();
        $config->setHost("https://cloud.staging.kiwigrid.com/rest/timeseriesreadservice/devices");
        $config->setAccessToken("eyJjbGllbnRfaWQiOiJ3dW1tLmRldmVsb3Blcl9hcHAiLCJ1c2VyX2lkIjoid3VtbV91c2VyIiwiY2xpZW50X3R5cGUiOiJBUFBMSUNBVElPTiIsInNlc3Npb25faWQiOiI3M2I4YmEwMy1jZjZkLTQyOWMtOWUzMy1lZGU5N2E4ZTY0ZDciLCJzZXNzaW9uIjp7fSwic2NvcGUiOlsicmVhZF90YWd2YWx1ZV9oaXN0b3J5IiwicmVhZF9kZXZpY2VzIl0sImFjY2Vzc2libGVfY2hhbm5lbHMiOlsid3VtbSJdLCJleHBpcmF0aW9uIjoxNTgyMTIwMjE4NTU5LCJjaGFubmVsIjoid3VtbSJ9LlMzMDEzUyszcDM0N25OUkNKYU9MZ09nazl3TT0");
        $config->setDebug(true);
        $config->setDebugFile("kiwigriddebug.log");

        $apiInstance = new \OpenAPI\Client\Api\TimeSeriesReadServiceApi(null, $config

        );
        $series_query_request = new \OpenAPI\Client\Model\SeriesQueryRequest(); // \OpenAPI\Client\Model\SeriesQueryRequest | Complex object consisting of timestamp interval (from,to), default aggregation config and unique GUIDs and multiple tags. Each GUID can contain several tags. Each tag contains aggregators with the corresponding config.
        $series_query_request->setFrom(1580544606);
        $series_query_request->setTo(1582099806);



        $aggregation_config  = [
            'resolution'            => 'PT240M',
            'function'              => 'TWA',
            'fill'                  => 'NONE',
            'useCreatedAtAsValue'   => false,
            'timeZoneId'            => 'UTC',
        ];
        $series_query_request->setDefaultAggregationConfig($aggregation_config);

        try {
            $result = $apiInstance->aggregateMultipleTagValuesForMultipleDevices($series_query_request);
            print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling TimeSeriesReadServiceApi->aggregateMultipleTagValuesForMultipleDevices: ', $e->getMessage(), PHP_EOL;
        }

        */
      
        $kiwi_import = new ImportKiwiGrid($this);
        $kiwi_import->importFromTo("2020-01-02 00:00:00", "2020-01-03 23:59:59");
        
          
    }
   
    
}
