<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBezugseinheitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bezugseinheit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("name",20)->index();
            $table->string("descr", 100)->nullable();
            $table->string("ctrluser", 40)->nullable();
            $table->timestamp("von")->useCurrent();
            $table->timestamp("bis")->useCurrent();
            $table->string("typ",2)->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bezugseinheit');
    }
}
