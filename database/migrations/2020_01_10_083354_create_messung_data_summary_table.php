<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessungDataSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messung_data_summary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->string("descr", 150)->nullable();
            $table->string("filename", 150)->nullable();
            $table->float("sum", 10)->nullable();
            $table->tinyInteger('unit_id')->nullable();
            $table->dateTimeTz('von')->nullable();
            $table->dateTimeTz('bis')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messung_data_summary');
    }
}
