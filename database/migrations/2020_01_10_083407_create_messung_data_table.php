<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessungDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messung_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->dateTimeTz('von');
            $table->dateTimeTz('bis');
            $table->dateTime('datum')->nullable();
            $table->unsignedBigInteger('summary_id')->nullable();
            // foreign key
            $table->unsignedBigInteger('messeinheit_id');
            $table->foreign('messeinheit_id')->references('id')->on('messeinheit');
            $table->foreign('summary_id')->references('id')->on('messung_data_summary')->onDelete('cascade');
            $table->decimal('amount',8,4);
            $table->string('typ', 3)->nullable();
            //$table->string('descr', 2)->nullable();
            $table->string('ctrluser', 100)->nullable();
            $table->date('ctrldate')->nullable();
            $table->string('ctrlstate', 2)->nullable();
            $table->integer('unit')->nullable();
            $table->tinyInteger('TZ')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messung_data');
    }
}
