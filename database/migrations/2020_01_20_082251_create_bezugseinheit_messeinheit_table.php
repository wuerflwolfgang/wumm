<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBezugseinheitMesseinheitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bezugseinheit_messeinheit', function (Blueprint $table) {
            //
            
            $table->bigInteger('bezugseinheit_id')->unsigned();
            $table->bigInteger('messeinheit_id')->unsigned();
            
            $table->foreign('bezugseinheit_id')->references('id')->on('bezugseinheit');//->onDelete('cascade');
            $table->foreign('messeinheit_id')->references('id')->on('messeinheit')->onDelete('cascade');
        });
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bezugseinheit_messeinheit');
    }
}
