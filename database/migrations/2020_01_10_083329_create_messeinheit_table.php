<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMesseinheitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messeinheit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            // Foreign key => messstelle
            // foreign key
            $table->string("name",20)->index();
            $table->unsignedBigInteger('messstelle_id');
            $table->foreign('messstelle_id')->references('id')->on('messstelle');
            $table->timestamp("von")->useCurrent();
            $table->timestamp("bis")->useCurrent();
            
            $table->string('typ', 2)->nullable();
            $table->string('descr', 100)->nullable();
            $table->string('ctrluser', 100)->nullable();
            $table->date('ctrldate')->nullable();
            $table->string('ctrlstate', 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messeinheit');
    }
}
