<?php

use App\Models\Person;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Models\Role;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker     = Faker\Factory::create();


        
        // Seed test admin
        $seededAdminEmail = 'udo.fischer@excello.eu';
        $person = Person::where('email', '=', $seededAdminEmail)->first();
        if (false) {
            $person = Person::create([
                'company_name'                   => "Excello GmbH",
                'first_name'                     => "Udo",
                'bezugseinheit_id'               => 1,
                'last_name'                      => "Fischer",
                'email'                          => $seededAdminEmail,
                'password'                       => Hash::make('password'),
                'token'                          => str_random(64),
                'activated'                      => true,
                'type'                           => 1,
                'street'                         => "Allendestr. 68",
                'zip'                            => "98574",
                'city'                           => "Schmalkalden",

            ]);

            $person->save();
        }


        // Seed test users
        // $user = factory(App\Models\Profile::class, 5)->create();
        // $users = User::All();
        // foreach ($users as $user) {
        //     if (!($user->isAdmin()) && !($user->isUnverified())) {
        //         $user->attachRole($userRole);
        //     }
        // }
    }
}
